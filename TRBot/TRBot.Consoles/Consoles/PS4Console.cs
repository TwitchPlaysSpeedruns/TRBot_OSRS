﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using TRBot.Parsing;
using TRBot.VirtualControllers;

namespace TRBot.Consoles
{
    /// <summary>
    /// The PlayStation 4.
    /// </summary>
    public sealed class PS4Console : GameConsole
    {
        public PS4Console()
        {
            Name = "ps4";

            Initialize();
        }

        private void Initialize()
        {
            SetConsoleInputs(new Dictionary<string, InputData>(39)
            {
                { "left",       InputData.CreateAxis("left", (int)GlobalAxisVals.AXIS_X, 0.5d, 0d, 0.5d) },
                { "right",      InputData.CreateAxis("right", (int)GlobalAxisVals.AXIS_X, 0.5d, 1d, 0.5d) },
                { "up",         InputData.CreateAxis("up", (int)GlobalAxisVals.AXIS_Y, 0.5d, 0d, 0.5d) },
                { "down",       InputData.CreateAxis("down", (int)GlobalAxisVals.AXIS_Y, 0.5d, 1d, 0.5d) },
                { "rleft",      InputData.CreateAxis("rleft", (int)GlobalAxisVals.AXIS_RX, 0.5d, 0d, 0.5d) },
                { "rright",     InputData.CreateAxis("rright", (int)GlobalAxisVals.AXIS_RX, 0.5d, 1d, 0.5d) },
                { "rup",        InputData.CreateAxis("rup", (int)GlobalAxisVals.AXIS_RY, 0.5d, 0d, 0.5d) },
                { "rdown",      InputData.CreateAxis("rdown", (int)GlobalAxisVals.AXIS_RY, 0.5d, 1d, 0.5d) },
                { "swipeleft",  InputData.CreateAxis("swipeleft", (int)GlobalAxisVals.AXIS_M1, 0.5d, 0d, 0.5d) },
                { "swiperight", InputData.CreateAxis("swiperight", (int)GlobalAxisVals.AXIS_M1, 0.5d, 1d, 0.5d) },
                { "swipeup",    InputData.CreateAxis("swipeup", (int)GlobalAxisVals.AXIS_M2, 0.5d, 0d, 0.5d) },
                { "swipedown",  InputData.CreateAxis("swipedown", (int)GlobalAxisVals.AXIS_M2, 0.5d, 1d, 0.5d) },

                { "dleft",      InputData.CreateButton("dleft", (int)GlobalButtonVals.BTN1) },
                { "dright",     InputData.CreateButton("dright", (int)GlobalButtonVals.BTN2) },
                { "dup",        InputData.CreateButton("dup", (int)GlobalButtonVals.BTN3) },
                { "ddown",      InputData.CreateButton("ddown", (int)GlobalButtonVals.BTN4) },
                { "square",     InputData.CreateButton("square", (int)GlobalButtonVals.BTN9) },
                { "triangle",   InputData.CreateButton("triangle", (int)GlobalButtonVals.BTN10) },
                { "circle",     InputData.CreateButton("circle", (int)GlobalButtonVals.BTN11) },
                { "cross",      InputData.CreateButton("cross", (int)GlobalButtonVals.BTN12) },
                { "share",      InputData.CreateButton("share", (int)GlobalButtonVals.BTN13) },
                { "options",    InputData.CreateButton("options", (int)GlobalButtonVals.BTN14) },
                { "l1",         InputData.CreateButton("l1", (int)GlobalButtonVals.BTN15) },
                { "r1",         InputData.CreateButton("r1", (int)GlobalButtonVals.BTN16) },
                { "l2",         InputData.CreateButton("l2", (int)GlobalButtonVals.BTN17) },
                { "r2",         InputData.CreateButton("r2", (int)GlobalButtonVals.BTN18) },
                { "l3",         InputData.CreateButton("l3", (int)GlobalButtonVals.BTN19) },
                { "r3",         InputData.CreateButton("r3", (int)GlobalButtonVals.BTN20) },
                { "touchclick", InputData.CreateButton("touchclick", (int)GlobalButtonVals.BTN21) },
                { "#",          InputData.CreateBlank("#") },
                { ".",          InputData.CreateBlank(".") },

                //Spare buttons
                { "sb1",        InputData.CreateButton("sb1", (int)GlobalButtonVals.BTN24) },
                { "sb2",        InputData.CreateButton("sb2", (int)GlobalButtonVals.BTN25) },
                { "sb3",        InputData.CreateButton("sb3", (int)GlobalButtonVals.BTN26) },
                { "sb4",        InputData.CreateButton("sb4", (int)GlobalButtonVals.BTN27) },
                { "sb5",        InputData.CreateButton("sb5", (int)GlobalButtonVals.BTN28) },
                { "sb6",        InputData.CreateButton("sb6", (int)GlobalButtonVals.BTN29) },
                { "sb7",        InputData.CreateButton("sb7", (int)GlobalButtonVals.BTN30) },
                { "sb8",        InputData.CreateButton("sb8", (int)GlobalButtonVals.BTN31) },
            });

            InvalidCombos = new List<InvalidCombo>();
        }
    }
}
