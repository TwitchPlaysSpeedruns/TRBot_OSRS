﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using TRBot.Parsing;
using TRBot.VirtualControllers;

namespace TRBot.Consoles
{
    /// <summary>
    /// The Nintendo Wii.
    /// </summary>
    public sealed class WiiConsole : GameConsole
    {
        /// <summary>
        /// The various extensions for the Wii.
        /// </summary>
        /// <remarks>Some peripherals, such as the Wii Balance Board, currently cannot be emulated at all (see Dolphin wiki).</remarks>
        public enum WiiInputExtensions
        {
            None = 0,
            Nunchuk = 1,
            ClassicController = 2,
            Guitar = 3,
            DrumKit = 4,
            DJTurntable = 5,
            uDrawGameTablet = 6,
            DrawsomeTablet = 7,
            TaikoDrum = 8
        }

        /// <summary>
        /// The current input mode for the Wii.
        /// </summary>
        public WiiInputExtensions InputExtension = WiiInputExtensions.Nunchuk;

        public WiiConsole()
        {
            Name = "wii";

            Initialize();
        }

        private void Initialize()
        {
            SetConsoleInputs(new Dictionary<string, InputData>(40) {
                { "left",       InputData.CreateAxis("left", (int)GlobalAxisVals.AXIS_X, 0.5d, 0d, 0.5d) },
                { "right",      InputData.CreateAxis("right", (int)GlobalAxisVals.AXIS_X, 0.5d, 1d, 0.5d) },
                { "up",         InputData.CreateAxis("up", (int)GlobalAxisVals.AXIS_Y, 0.5d, 0d, 0.5d) },
                { "down",       InputData.CreateAxis("down", (int)GlobalAxisVals.AXIS_Y, 0.5d, 1d, 0.5d) },
                { "tleft",      InputData.CreateAxis("tleft", (int)GlobalAxisVals.AXIS_RX, 0.5d, 0d, 0.5d) },
                { "tright",     InputData.CreateAxis("tright", (int)GlobalAxisVals.AXIS_RX, 0.5d, 1d, 0.5d) },
                { "tforward",   InputData.CreateAxis("tforward", (int)GlobalAxisVals.AXIS_RY, 0.5d, 0d, 0.5d) },
                { "tback",      InputData.CreateAxis("tback", (int)GlobalAxisVals.AXIS_RY, 0.5d, 1d, 0.5d) },
                { "pleft",      InputData.CreateAxis("pleft", (int)GlobalAxisVals.AXIS_RZ, 0.5d, 0d, 0.5d) },
                { "pright",     InputData.CreateAxis("pright", (int)GlobalAxisVals.AXIS_RZ, 0.5d, 1d, 0.5d) },
                { "pup",        InputData.CreateAxis("pup", (int)GlobalAxisVals.AXIS_Z, 0.5d, 0d, 0.5d) },
                { "pdown",      InputData.CreateAxis("pdown", (int)GlobalAxisVals.AXIS_Z, 0.5d, 1d, 0.5d) },
                
                { "dleft",      InputData.CreateButton("dleft", (int)GlobalButtonVals.BTN1) },
                { "dright",     InputData.CreateButton("dright", (int)GlobalButtonVals.BTN2) },
                { "dup",        InputData.CreateButton("dup", (int)GlobalButtonVals.BTN3) },
                { "ddown",      InputData.CreateButton("ddown", (int)GlobalButtonVals.BTN4) },              
                { "a",          InputData.CreateButton("a", (int)GlobalButtonVals.BTN9) },
                { "b",          InputData.CreateButton("b", (int)GlobalButtonVals.BTN10) },
                { "one",        InputData.CreateButton("one", (int)GlobalButtonVals.BTN11) },
                { "two",        InputData.CreateButton("two", (int)GlobalButtonVals.BTN12) },
                { "minus",      InputData.CreateButton("minus", (int)GlobalButtonVals.BTN13) },
                { "plus",       InputData.CreateButton("plus", (int)GlobalButtonVals.BTN14) },
                { "c",          InputData.CreateButton("c", (int)GlobalButtonVals.BTN15) },
                { "z",          InputData.CreateButton("z", (int)GlobalButtonVals.BTN16) },  
                { "shake",      InputData.CreateButton("shake", (int)GlobalButtonVals.BTN17) },
                { "point",      InputData.CreateButton("point", (int)GlobalButtonVals.BTN18) },
                { "ss1",        InputData.CreateButton("ss1", (int)GlobalButtonVals.BTN21) },
                { "ss2",        InputData.CreateButton("ss2", (int)GlobalButtonVals.BTN22) },
                { "ss3",        InputData.CreateButton("ss3", (int)GlobalButtonVals.BTN23) },
                { "ss4",        InputData.CreateButton("ss4", (int)GlobalButtonVals.BTN24) },
                { "ss5",        InputData.CreateButton("ss5", (int)GlobalButtonVals.BTN25) },
                { "ss6",        InputData.CreateButton("ss6", (int)GlobalButtonVals.BTN26) },
                { "ls1",        InputData.CreateButton("ls1", (int)GlobalButtonVals.BTN27) },
                { "ls2",        InputData.CreateButton("ls2", (int)GlobalButtonVals.BTN28) },
                { "ls3",        InputData.CreateButton("ls3", (int)GlobalButtonVals.BTN29) },
                { "ls4",        InputData.CreateButton("ls4", (int)GlobalButtonVals.BTN30) },
                { "ls5",        InputData.CreateButton("ls5", (int)GlobalButtonVals.BTN31) },
                { "ls6",        InputData.CreateButton("ls6", (int)GlobalButtonVals.BTN32) },
                { "#",          InputData.CreateBlank("#") },
                { ".",          InputData.CreateBlank(".") }
            });

            InvalidCombos = new List<InvalidCombo>();
        }
    }
}
