﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using TRBot.Utilities;
using static TRBot.Consoles.ConsoleConstants;

namespace TRBot.Consoles
{
    /// <summary>
    /// The types of inputs.
    /// This is a bitwise field.
    /// </summary>
    [Flags]
    public enum InputTypes
    {
        Blank = 0,
        Button = 1 << 0,
        Axis = 1 << 1
    }

    /// <summary>
    /// Represents input data.
    /// </summary>
    public class InputData
    {
        /// <summary>
        /// The ID of the input.
        /// </summary>
        public int ID { get; set; } = 0;

        /// <summary>
        /// The console ID the input belongs to.
        /// </summary>
        public int ConsoleID { get; set; } = 0;

        /// <summary>
        /// The access level of the input.
        /// </summary>
        public long Level { get; set; } = 0;

        /// <summary>
        /// The name of the input.
        /// </summary>
        public string Name { get; set; } = string.Empty;
        
        /// <summary>
        /// The button value for the input.
        /// </summary>
        public int ButtonValue { get; set; } = 0;

        /// <summary>
        /// The axis value for the input.
        /// </summary>
        public int AxisValue { get; set; } = 0;

        /// <summary>
        /// The type of input this input is. An input can be more than one type.
        /// </summary>
        public InputTypes InputType { get; set; } = InputTypes.Blank;

        /// <summary>
        /// The minimum value of the axis, normalized from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
        /// </summary>
        public double MinAxisVal { get; set; } = 0d;

        /// <summary>
        /// The maximum value of the axis, normalized from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
        /// </summary>
        public double MaxAxisVal { get; set; } = 1d;

        /// <summary>
        /// The maximum percent the axis for this input can be pressed.
        /// If pressed above this amount, it's considered a button press.
        /// <para>This is useful only for axes and buttons with shared names. 
        /// For example, the GameCube's "L" and "R" inputs function both as axes and buttons.</para>
        /// </summary>
        public double MaxAxisPercent { get; set; } = 100d;

        /// <summary>
        /// The default value of the axis, or its at rest state, normalized from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
        /// </summary>
        public double DefaultAxisVal { get; set; } = 0.5d;

        /// <summary>
        /// Whether the input is enabled.
        /// </summary>
        public long Enabled { get; set; } = 1;

        /// <summary>
        /// The GameConsole associated with this input.
        /// This is used by the database and should not be assigned or modified manually.
        /// </summary>
        public virtual GameConsole Console { get; set; } = null;

        /// <summary>
        /// A helper property to get the input's enabled state.
        /// </summary>
        public bool IsEnabled => (Enabled > 0);

        public InputData()
        {

        }

        public InputData(string name, in int buttonValue, in int axisValue, in InputTypes inputType,
            in double minAxisVal, in double maxAxisVal, in double maxAxisPercent, in double defaultAxisVal)
        {
            Name = name;
            ButtonValue = buttonValue;
            AxisValue = axisValue;
            InputType = inputType;
            MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxAxisPercent = Math.Clamp(maxAxisPercent, 0d, 100d);
        }

        public InputData(string name, in int buttonValue, in int axisValue, in InputTypes inputType,
            in double minAxisVal, in double maxAxisVal, in double maxAxisPercent, in double defaultAxisVal, in long inputLevel)
            : this(name, buttonValue, axisValue, inputType, minAxisVal, maxAxisVal, maxAxisPercent, defaultAxisVal)
        {
            Level = inputLevel;
        }

        public void UpdateData(in InputData inputData)
        {
            Name = inputData.Name;
            ButtonValue = inputData.ButtonValue;
            AxisValue = inputData.AxisValue;
            InputType = inputData.InputType;
            MinAxisVal = inputData.MinAxisVal;
            MaxAxisVal = inputData.MaxAxisVal;
            MaxAxisPercent = inputData.MaxAxisPercent;
            DefaultAxisVal = inputData.DefaultAxisVal;
        }

        public override string ToString()
        {
            return $"Name: \"{Name}\" | {nameof(ConsoleID)}: {ConsoleID} | BtnVal: {ButtonValue} | AxisVal: {AxisValue} | InputType: {(int)InputType} ({InputType}) | MinAxis: {MinAxisVal} | MaxAxis: {MaxAxisVal} | MaxAxisPercent: {MaxAxisPercent} | DefaultAxisVal: {DefaultAxisVal} | Level: {Level} | Enabled: {IsEnabled}";
        }

        public static InputData CreateBlank(string name)
        {
            InputData blankInput = new InputData();
            blankInput.Name = name;
            blankInput.InputType = InputTypes.Blank;

            return blankInput;
        }

        public static InputData CreateButton(string name, in int buttonValue)
        {
            InputData btnData = new InputData();
            btnData.Name = name;
            btnData.ButtonValue = buttonValue;
            btnData.InputType = InputTypes.Button;

            return btnData;
        }

        public static InputData CreateAxis(string name, in int axisValue, in double minAxisVal, in double maxAxisVal,
            in double defaultAxisVal)
        {
            InputData btnData = new InputData();
            btnData.Name = name;
            btnData.AxisValue = axisValue;
            btnData.InputType = InputTypes.Axis;
            btnData.MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.MaxAxisPercent = 100d;

            return btnData;
        }

        public static InputData CreateAxis(string name, in int axisValue, in double minAxisVal, in double maxAxisVal,
            in double maxAxisPercent, in double defaultAxisVal)
        {
            InputData btnData = new InputData();
            btnData.Name = name;
            btnData.AxisValue = axisValue;
            btnData.InputType = InputTypes.Axis;
            btnData.MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.MaxAxisPercent = Math.Clamp(maxAxisPercent, 0d, 100d);

            return btnData;
        }
    }

    /// <summary>
    /// Represents an input axis. Min and max axis values are normalized in the range <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
    /// </summary>
    public struct InputAxis
    {
        /// <summary>
        /// The value of the axis.
        /// </summary>
        public int AxisVal;

        /// <summary>
        /// The minimum value of the axis, normalized.
        /// </summary>
        public double MinAxisVal;

        /// <summary>
        /// The maximum value of the axis, normalized.
        /// </summary>
        public double MaxAxisVal;

        /// <summary>
        /// The maximum percent this axis can be pressed. If pressed above this amount, it's considered a button press.
        /// <para>This is useful only for axes and buttons with shared names. 
        /// For example, the GameCube's "L" and "R" inputs function both as axes and buttons.</para>
        /// </summary>
        public double MaxPercentPressed;

        /// <summary>
        /// The default value of the axis, normalized.
        /// </summary>
        public double DefaultAxisVal;

        /// <summary>
        /// Constructs an input axis.
        /// </summary>
        /// <param name="axisVal">The value of the input axis.</param>
        /// <param name="minAxisVal">The normalized minimum value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        /// <param name="maxAxisVal">The normalized maximum value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        /// <param name="defaultAxisVal">The normalized default value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        public InputAxis(in int axisVal, in double minAxisVal, in double maxAxisVal, in double defaultAxisVal)
        {
            AxisVal = axisVal;
            MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxPercentPressed = 100d;
        }

        /// <summary>
        /// Constructs an input axis.
        /// </summary>
        /// <param name="axisVal">The value of the input axis.</param>
        /// <param name="minAxisVal">The normalized minimum value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        /// <param name="maxAxisVal">The normalized maximum value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        /// <param name="maxPercentPressed">The maximum percent the axis can be pressed. This is clamped from 0 to 100.</param>
        /// <param name="defaultAxisVal">The normalized default value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        public InputAxis(in int axisVal, in double minAxisVal, in double maxAxisVal, in double maxPercentPressed, in double defaultAxisVal)
        {
            AxisVal = axisVal;
            MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxPercentPressed = Math.Clamp(maxPercentPressed, 0d, 100d);
        }

        public override bool Equals(object obj)
        {
            if (obj is InputAxis inputAxis)
            {
                return (this == inputAxis);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = (hash * 23) + AxisVal.GetHashCode();
                hash = (hash * 23) + MinAxisVal.GetHashCode();
                hash = (hash * 23) + MaxAxisVal.GetHashCode();
                hash = (hash * 23) + MaxPercentPressed.GetHashCode();
                hash = (hash * 23) + DefaultAxisVal.GetHashCode();
                return hash;
            }
        }

        public static bool operator==(InputAxis a, InputAxis b)
        {
            return (a.AxisVal == b.AxisVal
                && Helpers.IsApproximate(a.MinAxisVal, b.MinAxisVal, 0.0009d)
                && Helpers.IsApproximate(a.MaxAxisVal, b.MaxAxisVal, 0.0009d)
                && Helpers.IsApproximate(a.MaxPercentPressed, b.MaxPercentPressed, 0.0009d)
                && Helpers.IsApproximate(a.DefaultAxisVal, b.DefaultAxisVal, 0.0009d));
        }

        public static bool operator!=(InputAxis a, InputAxis b)
        {
            return !(a == b);
        }
    }

    /// <summary>
    /// Represents an input button.
    /// </summary>
    public struct InputButton
    {
        /// <summary>
        /// The value of the button.
        /// </summary>
        public uint ButtonVal;

        public InputButton(in uint buttonVal)
        {
            ButtonVal = buttonVal;
        }

        public override bool Equals(object obj)
        {
            if (obj is InputButton inputBtn)
            {
                return (this == inputBtn);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 13;
                hash = (hash * 17) + ButtonVal.GetHashCode();
                return hash;
            }
        }

        public static bool operator==(InputButton a, InputButton b)
        {
            return (a.ButtonVal == b.ButtonVal);
        }

        public static bool operator!=(InputButton a, InputButton b)
        {
            return !(a == b);
        }
    }
}
