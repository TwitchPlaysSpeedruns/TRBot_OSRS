﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Net;

namespace TRBot.WebSocket
{
    /// <summary>
    /// Manages WebSocket client and server connections.
    /// </summary>
    public interface IWebSocketManager : IDisposable
    {
        bool IsServerListening { get; }
        bool IsServerSecure { get; }

        IPAddress ServerIPAddress { get; }
        string ServerUrl { get; }
        int ServerPort { get; }

        event WebSocketServerStarted ServerStarted;
        event WebSocketServerStopped ServerStopped;

        bool StartServer(string addressURL);
        void StopServer();
        bool AddServerService<T>(string path) where T : WebSocketService, new();
        bool RemoveServerService(string path);

        void ServerBroadcastAsync(string path, string data, Action onCompleted, Action<Exception> onError);
    }
}
