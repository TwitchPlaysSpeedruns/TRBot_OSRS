﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Logging;

namespace TRBot.Misc
{
    /// <summary>
    /// Handles messages with rate limiting.
    /// </summary>
    public interface IBotMessageHandler
    {
        /// <summary>
        /// The ClientService the message handler is using.
        /// </summary>
        IClientService ClientService { get; }

        /// <summary>
        /// Whether to also log bot messages to the logger.
        /// </summary>
        bool LogToLogger { get; }

        /// <summary>
        /// How many messages are in the queue.
        /// </summary>
        int ClientMessageCount { get; }

        /// <summary>
        /// The string to prepend to each message.
        /// </summary>
        string MessagePrefix { get; }

        /// <summary>
        /// The message throttler that restricts how often messages are sent.
        /// </summary>
        IBotMessageThrottler MessageThrottler { get; }

        /// <summary>
        /// The current message throttling option.
        /// </summary>
        MessageThrottlingOptions CurThrottleOption { get; }

        void CleanUp();

        void SetChannelName(string channelName);

        void SetMessagePrefix(string messagePrefix);

        void SetMessageThrottling(in MessageThrottlingOptions msgThrottleOption, in MessageThrottleData messageThrottleData);

        void SetLogToLogger(in bool logToLogger);

        void Update(in DateTime nowUTC);

        /// <summary>
        /// Sends the next queued message through the client service. This returns false if this fails.
        /// </summary>
        /// <returns>true if the message was successfully sent. false if the client service is disconnected or the message fails to send.</returns>
        bool SendNextQueuedMessage();

        void QueueMessage(string message);

        void QueueMessage(string message, in Serilog.Events.LogEventLevel logLevel);

        void QueueMessageSplit(string message, in int maxCharCount, string separator);

        void QueueMessageSplit(string message, in Serilog.Events.LogEventLevel logLevel, in int maxCharCount, string separator);
    }
}
