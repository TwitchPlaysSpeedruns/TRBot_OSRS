﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Parsing;
using TRBot.Consoles;

namespace TRBot.Misc
{
    /// <summary>
    /// Handles post-processing inputs.
    /// </summary>
    public static class ParserPostProcess
    {
        /// <summary>
        /// Inserts artificial blank inputs after input subsequences if blank inputs do not exist or aren't the longest duration in the subsequence.
        /// This is used to add artificial delays after each input.
        /// The given game console must have a blank input defined for this to work.
        /// <para>
        /// This is expected to go over the maximum input duration, as the delays are inserted after the input.
        /// Otherwise, it'll be cumbersome as players will have to account for the delays when writing input sequences.
        /// That said, aim to keep the <paramref name="midInputDelay"> a low value.
        /// </para>
        /// </summary>
        /// <param name="inputSequence">The parsed input sequence.</param>
        /// <param name="defaultPort">The default controller port to use for the new inputs.</param>
        /// <param name="midInputDelay">The duration of the inserted blank inputs, in milliseconds.</param>
        /// <param name="gameConsole">The game console to validate blank inputs for.</param>
        /// <param name="validBlankInput">A validated blank input.</param>
        /// <returns>Data regarding the mid input delays.</returns>
        public static MidInputDelayData InsertMidInputDelays(in ParsedInputSequence inputSequence,
            in int defaultPort, in int midInputDelay, GameConsole gameConsole, InputData validBlankInput)
        {
            //There aren't enough inputs to add delays to
            if (inputSequence.Inputs == null || inputSequence.Inputs.Count < 2)
            {
                return new MidInputDelayData(null, inputSequence.TotalDuration, false, "There aren't enough inputs to insert delays.");
            }

            //No input
            if (validBlankInput == null)
            {
                return new MidInputDelayData(null, inputSequence.TotalDuration, false, $"{nameof(validBlankInput)} is null.");
            }

            //Copy the input sequence
            List<List<ParsedInput>> parsedInputs = new List<List<ParsedInput>>(inputSequence.Inputs.Count);
            
            bool lastIndexBlankLongestDur = true;
            int additionalDelay = 0;

            //Insert sequences in the same loop as copying to improve speed
            //Check if there are any waits in between two input subsequences
            //If not, add the delay first then the next subsequence
            for (int i = 0; i < inputSequence.Inputs.Count; i++)
            {
                List<ParsedInput> curList = inputSequence.Inputs[i];
                List<ParsedInput> newListCopy = new List<ParsedInput>(curList.Count);
                int longestDur = 0;
                bool blankHasLongestDur = false;

                for (int j = 0; j < curList.Count; j++)
                {
                    ParsedInput curInput = curList[j];
                    newListCopy.Add(curInput);

                    //Check for the same duration for consistent behavior
                    //If the blank input lasts as long as another input, it should still add a delay
                    if (curInput.Duration >= longestDur)
                    {
                        longestDur = curInput.Duration;
                        blankHasLongestDur = false;

                        if (gameConsole.IsBlankInput(curInput) == true)
                        {
                            blankHasLongestDur = true;
                        }
                    }
                }

                //Add a delay input in between
                if (blankHasLongestDur == false && lastIndexBlankLongestDur == false)
                {
                    ParsedInput newDelayInput = new ParsedInput(validBlankInput.Name, false, false, 100, midInputDelay, InputDurationTypes.Milliseconds, defaultPort);
                    
                    parsedInputs.Add(new List<ParsedInput>(1) { newDelayInput });

                    additionalDelay += midInputDelay;
                }

                parsedInputs.Add(newListCopy);

                lastIndexBlankLongestDur = blankHasLongestDur;
            }

            return new MidInputDelayData(parsedInputs, inputSequence.TotalDuration + additionalDelay, true, string.Empty);
        }
    }
}
