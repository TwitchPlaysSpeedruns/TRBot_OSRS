﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using TRBot.Consoles;
using TRBot.Parsing;
using TRBot.Utilities;
using TRBot.VirtualControllers;
using TRBot.Logging;

namespace TRBot.Misc
{
    /// <summary>
    /// A parser validator for the <see cref="StandardParser"/>.
    /// </summary>
    public class StandardParserValidator : IParserValidator
    {
        public Dictionary<string, int> RestrictedInputs { get; private set; } = null;
        public long UserLevel { get; private set; } = 0L;
        public GameConsole UsedConsole { get; private set; } = null;
        public IVirtualControllerManager VControllerMngr { get; private set; } = null;

        private Dictionary<string, InputData> InputPermissionLevels = null;
        private List<InvalidCombo> InvalidCombos = null;

        private StringBuilder ComboStrBuilder = new StringBuilder(128);
        private Dictionary<int, List<string>> CurrentComboDict = null;
        private Dictionary<int, List<string>> SubComboDict = null;

        private ITRBotLogger Logger = null;

        public StandardParserValidator(Dictionary<string,int> restrictedInputs,
            in long userLevel, GameConsole usedConsole, IVirtualControllerManager vControllerMngr,
            ITRBotLogger logger)
        {
            RestrictedInputs = restrictedInputs;
            UserLevel = userLevel;
            UsedConsole = usedConsole;
            VControllerMngr = vControllerMngr;

            InputPermissionLevels = UsedConsole.ConsoleInputs;
            InvalidCombos = UsedConsole.InvalidCombos;

            Logger = logger;

            Initialize();
        }

        private void Initialize()
        {
            int controllerCount = VControllerMngr.ControllerCount;

            //These dictionaries are for each controller port
            if (CurrentComboDict == null)
            {
                CurrentComboDict = new Dictionary<int, List<string>>(controllerCount);
            }

            if (SubComboDict == null)
            {
                SubComboDict = new Dictionary<int, List<string>>(controllerCount);
            }
            
            //Add pressed inputs from each controller
            for (int i = 0; i < controllerCount; i++)
            {
                IVirtualController controller = VControllerMngr.GetController(i);
                if (controller.IsAcquired == false)
                {
                    continue;
                }

                //Add already pressed inputs from all controllers
                for (int j = 0; j < InvalidCombos.Count; j++)
                {
                    string inputName = InvalidCombos[j].Input.Name;

                    //Check if the button exists and is pressed
                    if (UsedConsole.GetButtonValue(inputName, out InputButton inputBtn) == true)
                    {
                        if (controller.GetButtonState(inputBtn.ButtonVal) == ButtonStates.Pressed)
                        {
                            if (CurrentComboDict.ContainsKey(i) == false)
                            {
                                CurrentComboDict[i] = new List<string>(InvalidCombos.Count);
                            }

                            CurrentComboDict[i].Add(inputName);
                        }
                    }
                    //Check if the axis exists and is pressed in any capacity
                    else if (UsedConsole.GetAxisValue(inputName, out InputAxis inputAxis) == true)
                    {
                        if (controller.GetAxisState(inputAxis.AxisVal, inputAxis.DefaultAxisVal) != inputAxis.DefaultAxisVal)
                        {
                            if (CurrentComboDict.ContainsKey(i) == false)
                            {
                                CurrentComboDict[i] = new List<string>(InvalidCombos.Count);
                            }

                            CurrentComboDict[i].Add(inputName);
                        }
                    }
                    else
                    {
                        Logger.Warning($"\"{inputName}\" is part of an invalid input combo but doesn't exist for {UsedConsole.Name}.");
                    }
                }
            }
        }

        public ParserValidationData ValidateInput(in ParsedInput parsedInput)
        {
            //Check for a valid controller port
            if (parsedInput.ControllerPort >= 0 && parsedInput.ControllerPort < VControllerMngr.ControllerCount)
            {
                //Check if the controller is acquired
                IVirtualController controller = VControllerMngr.GetController(parsedInput.ControllerPort);
                if (controller.IsAcquired == false)
                {
                    return new ParserValidationData(ParserValidationTypes.Failed, $"(ERROR) Joystick number {parsedInput.ControllerPort + 1} with controller ID of {controller.ControllerID} has not been acquired! Ensure you, the host, have a virtual controller set up at this ID (double check permissions)");
                }
            }
            //Invalid port
            else
            {
                return new ParserValidationData(ParserValidationTypes.Failed, $"Invalid joystick number {parsedInput.ControllerPort + 1}. Joystick count = {VControllerMngr.ControllerCount}. Please change yours or your input's controller port to a valid number to perform inputs");
            }

            //Check if this input is restricted
            if (RestrictedInputs != null && RestrictedInputs.ContainsKey(parsedInput.Name) == true)
            {
                return new ParserValidationData(ParserValidationTypes.Failed, $"You're restricted from using input \"{parsedInput.Name}\"");
            }

            //Check permission level
            if (InputPermissionLevels != null
                && InputPermissionLevels.TryGetValue(parsedInput.Name, out InputData inputData) == true
                && UserLevel < inputData.Level)
            {
                return new ParserValidationData(ParserValidationTypes.Failed,
                    $"No permission to use input \"{parsedInput.Name}\", which requires at least level {inputData.Level}");
            }

            //End with validating input combos
            return ValidateInputCombos(parsedInput);
        }

        private bool InvalidComboContainsInputName(string inputName)
        {
            for (int i = 0; i < InvalidCombos.Count; i++)
            {
                if (InvalidCombos[i].Input.Name == inputName)
                {
                    return true;
                }
            }

            return false;
        }

        private ParserValidationData ValidateInputCombos(in ParsedInput parsedInput)
        {
            //If all these inputs are somehow pressed already, whatever we do now doesn't matter 
            //However, returning false here would prevent any further inputs from working, so
            //give a chance to check other inputs, such as releasing
            
            //Get controller port and initialize
            int port = parsedInput.ControllerPort;

            //Ensure a currentcombo entry is available for this port
            //Current and sub combo lists
            if (CurrentComboDict.TryGetValue(port, out List<string> currentCombo) == false)
            {
                currentCombo = new List<string>(InvalidCombos.Count);
                CurrentComboDict[port] = currentCombo;
            }

            //Ensure a subcombo entry is available for this port
            if (SubComboDict.TryGetValue(port, out List<string> subCombo) == false)
            {
                subCombo = new List<string>(InvalidCombos.Count);
                SubComboDict[port] = subCombo;
            }

            //Check if this input is in the invalid combo
            if (InvalidComboContainsInputName(parsedInput.Name) == false)
            {
                return new ParserValidationData(ParserValidationTypes.Passed, string.Empty);
            }
            
            //If it's not a release input and isn't in the held or current inputs, add it
            if (parsedInput.Release == false && subCombo.Contains(parsedInput.Name) == false
                && currentCombo.Contains(parsedInput.Name) == false)
            {
                subCombo.Add(parsedInput.Name);
                
                //Check the count after adding
                if ((subCombo.Count + currentCombo.Count) == InvalidCombos.Count)
                {
                    ComboStrBuilder.Clear();

                    //Make the message mention which inputs aren't allowed
                    ComboStrBuilder.Append("Inputs (");

                    for (int k = 0; k < InvalidCombos.Count; k++)
                    {
                        ComboStrBuilder.Append('"').Append(InvalidCombos[k].Input.Name).Append('"');

                        if (k < (InvalidCombos.Count - 1))
                        {
                            ComboStrBuilder.Append(',').Append(' ');
                        }
                    }

                    ComboStrBuilder.Append(") are not allowed to be pressed at the same time.");

                    return new ParserValidationData(ParserValidationTypes.Failed, ComboStrBuilder.ToString());
                }
            }
            
            //For holds, use the held combo
            if (parsedInput.Hold == true)
            {
                if (currentCombo.Contains(parsedInput.Name) == false)
                {
                    //Remove from the subcombo to avoid duplicates
                    currentCombo.Add(parsedInput.Name);
                    subCombo.Remove(parsedInput.Name);
                    
                    if ((currentCombo.Count + subCombo.Count) == InvalidCombos.Count)
                    {
                        ComboStrBuilder.Clear();

                        //Make the message mention which inputs aren't allowed
                        ComboStrBuilder.Append("Inputs (");

                        for (int k = 0; k < InvalidCombos.Count; k++)
                        {
                            ComboStrBuilder.Append('"').Append(InvalidCombos[k]).Append('"');

                            if (k < (InvalidCombos.Count - 1))
                            {
                                ComboStrBuilder.Append(',').Append(' ');
                            }
                        }

                        ComboStrBuilder.Append(") are not allowed to be pressed at the same time.");

                        return new ParserValidationData(ParserValidationTypes.Failed, ComboStrBuilder.ToString());
                    }
                }
            }
            //If released, remove from the current combo
            else if (parsedInput.Release == true)
            {
                currentCombo.Remove(parsedInput.Name);
            }

            return new ParserValidationData(ParserValidationTypes.Passed, string.Empty);
        }
    }
}
