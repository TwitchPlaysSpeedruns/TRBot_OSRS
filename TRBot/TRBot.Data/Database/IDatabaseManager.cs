/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Microsoft.EntityFrameworkCore;
using static TRBot.Data.DatabaseDelegates;

namespace TRBot.Data
{
    /// <summary>
    /// Helps manage a database.
    /// </summary>
    public interface IDatabaseManager<T> where T : DbContext
    {
        /// <summary>
        /// The path to the database file.
        /// </summary>
        string DatabasePath { get; }

        /// <summary>
        /// Opens a database context and returns it.
        /// The caller is responsible for disposing the context. 
        /// </summary>
        /// <returns>An opened DbContext.</returns>
        T OpenContext();

        /// <summary>
        /// Opens the database context, invokes an action, then disposes the context.
        /// </summary>
        /// <param name="dbContextAction">The action to perform on the database context.</param>
        void OpenCloseContext(DBContextAction<T> dbContextAction);

        /// <summary>
        /// Opens up a database context and applies any migrations.
        /// If the database does not exist, it will be created.
        /// </summary>
        void InitAndMigrateContext();
    }
}