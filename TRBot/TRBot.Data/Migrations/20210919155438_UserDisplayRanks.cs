﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TRBot.Data.Migrations
{
    public partial class UserDisplayRanks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "userdisplayranks");

            migrationBuilder.CreateTable(
                name: "UserDisplayRanks",
                schema: "userdisplayranks",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Rank = table.Column<long>(type: "INTEGER", nullable: false, defaultValue: 0L),
                    ExpRequirement = table.Column<long>(type: "INTEGER", nullable: false, defaultValue: 0L),
                    Label = table.Column<string>(type: "TEXT", nullable: true, defaultValue: "")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDisplayRanks", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserDisplayRanks_Rank",
                schema: "userdisplayranks",
                table: "UserDisplayRanks",
                column: "Rank",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDisplayRanks",
                schema: "userdisplayranks");
        }
    }
}
