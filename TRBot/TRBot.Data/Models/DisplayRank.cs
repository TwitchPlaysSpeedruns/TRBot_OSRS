﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Data
{
    /// <summary>
    /// A display rank.
    /// </summary>
    public class DisplayRank
    {
        /// <summary>
        /// The command's ID.
        /// </summary>
        public int ID { get; set; } = 0;

        /// <summary>
        /// The rank number.
        /// </summary>
        public long Rank { get; set; } = 0;

        /// <summary>
        /// The amount of experience required to reach the rank.
        /// </summary>
        public long ExpRequirement { get; set; } = 0L;

        /// <summary>
        /// The label associated with the rank.
        /// </summary>
        public string Label { get; set; } = string.Empty;

        public DisplayRank()
        {

        }

        public DisplayRank(in long rank, in long expRequirement, string label)
        {
            Rank = rank;
            ExpRequirement = expRequirement;
            Label = label;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Label) == true)
            {
                return $"(Rank {Rank})";
            }
            else
            {
                return $"(Rank {Rank} - {Label})";
            }
        }
    }
}
