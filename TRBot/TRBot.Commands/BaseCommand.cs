﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Connection;
using TRBot.Data;
using TRBot.Routines;
using TRBot.Misc;
using TRBot.Logging;
using TRBot.VirtualControllers;
using TRBot.Events;
using TRBot.Utilities;
using TRBot.WebSocket;

namespace TRBot.Commands
{
    /// <summary>
    /// Base class for a command.
    /// </summary>
    public abstract class BaseCommand
    {
        public bool Enabled = true;
        public string ClassName = string.Empty;
        public bool DisplayInHelp = true;
        public long Level = 0;
        public string ValueStr = string.Empty;

        public ICommandHandler CmdHandler { get; protected set; }= null;
        public IRoutineHandler RoutineHandler { get; protected set; } = null;
        public ITRBotLogger Logger { get; protected set; } = null;
        public IDatabaseManager<BotDBContext> DatabaseMngr { get; protected set; } = null;
        public IBotMessageHandler MessageHandler { get; protected set; } = null;
        public IDataReloader DataReloader { get; protected set; } = null;
        public IVirtualControllerContainer VControllerContainer { get; protected set; } = null;
        public IEventDispatcher EvtDispatcher { get; protected set; } = null;
        public IWebSocketManager WebSocketMngr { get; protected set; } = null;
        public IInputHandler InputHndlr { get; protected set; } = null;

        public BaseCommand()
        {
            
        }

        /// <summary>
        /// Sets required data for many commands to function.
        /// </summary>
        public void SetRequiredData(ICommandHandler cmdHandler, IRoutineHandler routineHandler,
            ITRBotLogger logger, IDatabaseManager<BotDBContext> databaseMngr, IBotMessageHandler messageHandler,
            IDataReloader dataReloader, IVirtualControllerContainer vControllerContainer,
            IEventDispatcher evtDispatcher, IWebSocketManager webSocketMngr, IInputHandler inputHandlr)
        {
            CmdHandler = cmdHandler;
            RoutineHandler = routineHandler;
            Logger = logger;
            DatabaseMngr = databaseMngr;
            MessageHandler = messageHandler;
            DataReloader = dataReloader;
            VControllerContainer = vControllerContainer;
            EvtDispatcher = evtDispatcher;
            WebSocketMngr = webSocketMngr;
            InputHndlr = inputHandlr;
        }

        public virtual void Initialize()
        {
            
        }

        public virtual void CleanUp()
        {
            
        }

        public abstract void ExecuteCommand(EvtChatCommandArgs args);

        protected void QueueMessage(string message)
        {
            MessageHandler.QueueMessage(message);
        }

        protected void QueueMessage(string message, in Serilog.Events.LogEventLevel logLevel)
        {
            MessageHandler.QueueMessage(message, logLevel);
        }

        protected void QueueMessageSplit(string message, in int maxCharCount, string separator)
        {
            MessageHandler.QueueMessageSplit(message, maxCharCount, separator);
        }

        protected void QueueMessageSplit(string message, in Serilog.Events.LogEventLevel logLevel,
            in int maxCharCount, string separator)
        {
            MessageHandler.QueueMessageSplit(message, logLevel, maxCharCount, separator);
        }
    }
}
