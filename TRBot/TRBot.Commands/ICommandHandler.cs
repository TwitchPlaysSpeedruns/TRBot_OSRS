﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Routines;
using TRBot.Permissions;
using TRBot.Logging;
using TRBot.Events;
using TRBot.WebSocket;
using TRBot.VirtualControllers;

namespace TRBot.Commands
{
    /// <summary>
    /// Manages commands.
    /// </summary>
    public interface ICommandHandler
    {
        void Initialize();

        void CleanUp();

        void HandleCommand(EvtChatCommandArgs args);

        BaseCommand GetCommand(string commandName);

        bool GetCommand<T>(out string commandName) where T : BaseCommand;

        bool AddCommand(string commandName, string commandTypeName, string valueStr,
            in long level, in bool commandEnabled, in bool displayInHelp);

        bool AddCommand(string commandName, BaseCommand command);

        bool RemoveCommand(string commandName);
    }
}
