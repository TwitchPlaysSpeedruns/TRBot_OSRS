﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;
using TRBot.Routines;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that toggles the periodic input message on or off.
    /// </summary>
    public sealed class TogglePeriodicInputMessageCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - no arguments (get value) or \"true\" or \"false\"";

        public TogglePeriodicInputMessageCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            //Ignore with too few arguments
            if (arguments.Count > 1)
            {
                QueueMessage(UsageMessage);
                return;
            }

            if (arguments.Count == 0)
            {
                long periodicInputVal = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.PERIODIC_INPUT_MESSAGE_ENABLED, 0L);
                string enabledStr = (periodicInputVal <= 0) ? "disabled" : "enabled";

                QueueMessage($"The periodic input message is currently {enabledStr}. To change the periodic input message enabled state, pass either \"true\" or \"false\" as an argument."); 
                return;
            }

            //Check for sufficient permissions
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = DataHelper.GetUserNoOpen(args.Command.ChatMessage.Username, context);
                if (user == null || user.HasEnabledAbility(PermissionConstants.SET_PERIODIC_INPUT_MESSAGE_ABILITY) == false)
                {
                    QueueMessage("You do not have the ability to change the periodic input message state!");
                    return;
                }
            }

            string newStateStr = arguments[0].ToLowerInvariant();

            if (bool.TryParse(newStateStr, out bool newState) == false)
            {
                QueueMessage("Invalid argument. To enable or disable the periodic input message, pass either \"true\" or \"false\" as an argument.");
                return;
            }

            long newVal = (newState == true) ? 1L : 0L;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings periodicInput = DataHelper.GetSettingNoOpen(SettingsConstants.PERIODIC_INPUT_MESSAGE_ENABLED, context);
                if (periodicInput == null)
                {
                    periodicInput = new Settings(SettingsConstants.PERIODIC_INPUT_MESSAGE_ENABLED, string.Empty, 1L);
                    context.SettingCollection.Add(periodicInput);

                    context.SaveChanges();
                }

                //Same value - don't make changes
                if ((periodicInput.ValueInt > 0 && newVal > 0) || (periodicInput.ValueInt <= 0 && newVal <= 0))
                {
                    QueueMessage("The periodic input message state is already this value!");
                    return;
                }

                periodicInput.ValueInt = newVal;

                context.SaveChanges();
            }

            if (newState == true)
            {
                QueueMessage("Enabled the periodic input message, which indicates when the periodic input sequence is performed.");
            }
            else
            {
                QueueMessage("Disabled the periodic input message.");
            }
        }
    }
}
