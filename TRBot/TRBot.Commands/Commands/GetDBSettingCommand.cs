﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;
using TRBot.Logging;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that gets a setting in the database.
    /// </summary>
    public sealed class GetDBSettingCommand : BaseCommand
    {
        private const string STRING_ARG = "string";
        private const string INT_ARG = "int";

        private string UsageMessage = $"Usage - \"setting key (string)\", \"string/int (for ValueStr/ValueInt)\"";

        public GetDBSettingCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 2)
            {
                QueueMessage(UsageMessage);
                return;
            }

            string settingKey = arguments[0];
            string typeStr = arguments[1].ToLowerInvariant();

            if (typeStr != STRING_ARG && typeStr != INT_ARG)
            {
                QueueMessage(UsageMessage);
                return;
            }

            Settings setting = DataHelper.GetSetting(DatabaseMngr, settingKey);

            if (setting == null)
            {
                QueueMessage($"No setting with a key of \"{settingKey}\" can be found in the database. Setting keys are case-sensitive.");
                return;
            }

            //Print the database setting
            string settingVal = (typeStr == INT_ARG) ? setting.ValueInt.ToString() : setting.ValueStr;

            int charLimit = (int)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.BOT_MSG_CHAR_LIMIT, 500L);

            QueueMessageSplit(settingVal, charLimit, " ");
        }
    }
}
