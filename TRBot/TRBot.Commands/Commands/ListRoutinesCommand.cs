﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Lists all routines.
    /// </summary>
    public sealed class ListRoutinesCommand : BaseCommand
    {
        private const string DISABLED_ARG = "disabled";
        private const string ALL_ARG = "all";

        private string UsageMessage = "Usage: no arguments (all enabled routines), \"disabled (optional)\" (only disabled routines), or \"all (optional)\" (enabled & disabled routines)";

        public ListRoutinesCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 1)
            {
                QueueMessage(UsageMessage);
                return;
            }

            string arg = string.Empty;

            if (arguments.Count > 0)
            {
                arg = arguments[0].ToLowerInvariant();

                //Validate argument
                if (arg != DISABLED_ARG && arg != ALL_ARG)
                {
                    QueueMessage(UsageMessage);
                    return;
                }
            }

            StringBuilder stringBuilder = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Show the routines that should be displayed based on our argument
                IQueryable<RoutineData> routineList = null;

                //All routines
                if (arg == ALL_ARG)
                {
                    routineList = context.Routines.AsQueryable();
                }
                //Disabled routines only
                else if (arg == DISABLED_ARG)
                {
                    routineList = context.Routines.Where(r => r.Enabled <= 0);
                }
                //Enabled routines only
                else
                {
                    routineList = context.Routines.Where(r => r.Enabled > 0);
                }
            
                //Order them alphabetically
                routineList = routineList.OrderBy(c => c.Name);
                int routineCount = routineList.Count();

                if (routineCount == 0)
                {
                    QueueMessage("There are no displayable routines based on your argument!");
                    return;
                }

                //The capacity is estimated by the number of routines times the average string length of each one
                stringBuilder = new StringBuilder(routineCount * 12);

                stringBuilder.Append("Hi ").Append(args.Command.ChatMessage.Username).Append(", here's the list of routines: ");

                foreach (RoutineData routine in routineList)
                {
                    stringBuilder.Append(routine.Name);

                    //Note if the routine is disabled
                    if (routine.Enabled <= 0)
                    {
                        stringBuilder.Append(" (disabled)");
                    }

                    stringBuilder.Append(',').Append(' ');
                }
            }

            stringBuilder.Remove(stringBuilder.Length - 2, 2);

            int msgCharLimit = (int)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.BOT_MSG_CHAR_LIMIT, 500L);

            QueueMessageSplit(stringBuilder.ToString(), msgCharLimit, ", ");
        }
    }
}
