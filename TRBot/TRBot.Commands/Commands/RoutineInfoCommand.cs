﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;
using TRBot.Routines;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that obtains information about a routine.
    /// </summary>
    public sealed class RoutineInfoCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"routine name\"";

        public RoutineInfoCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            //Ignore with incorrect number of arguments
            if (arguments.Count != 1)
            {
                QueueMessage(UsageMessage);
                return;
            }

            string routineName = arguments[0].ToLowerInvariant();

            BaseRoutine routine = RoutineHandler.FindRoutine(routineName);

            if (routine == null)
            {
                QueueMessage($"Routine \"{routineName}\" not found. If you added or removed routines, update with the reload command.");
                return;
            }

            //Show information about the routine
            QueueMessage($"\"{routineName}\" - Type: \"{routine.ClassName}\" | Enabled: {routine.Enabled} | ResetOnReload: {routine.ResetOnReload} | ValueStr: {routine.ValueStr}");
        }
    }
}
