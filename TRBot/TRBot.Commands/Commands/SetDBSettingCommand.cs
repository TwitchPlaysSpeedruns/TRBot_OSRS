﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;
using TRBot.Logging;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that sets a setting's values in the database.
    /// If the setting doesn't exist, it will be added.
    /// </summary>
    public sealed class SetDBSettingCommand : BaseCommand
    {
        private const string STRING_ARG = "string";
        private const string INT_ARG = "int";

        private const string NULL_ARG = "null";

        private string UsageMessage = $"Usage - \"setting key (string)\", \"string/int (for ValueStr/ValueInt)\", \"string/int (value to set) - \"null\" for a null ValueStr\"";

        public SetDBSettingCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 3)
            {
                QueueMessage(UsageMessage);
                return;
            }

            string settingKey = arguments[0];
            string typeStr = arguments[1].ToLowerInvariant();

            if (typeStr != STRING_ARG && typeStr != INT_ARG)
            {
                QueueMessage(UsageMessage);
                return;
            }

            bool isInteger = (typeStr == INT_ARG);

            string newSettingValue = arguments[2];
            int newSettingInt = 0;

            if (isInteger == true && int.TryParse(newSettingValue, out newSettingInt) == false)
            {
                QueueMessage("Given value is not an integer!");
                return;
            }

            bool addedNew = false;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings setting = DataHelper.GetSettingNoOpen(settingKey, context);

                if (setting == null)
                {
                    setting = new Settings(settingKey, null, 0);
                    context.SettingCollection.Add(setting);
                    
                    addedNew = true;
                }

                //Change ValueInt if we're adding an integer, otherwise change ValueStr
                if (isInteger == true)
                {
                    setting.ValueInt = newSettingInt;
                }
                else
                {
                    setting.ValueStr = (newSettingValue == NULL_ARG) ? string.Empty : newSettingValue;
                }

                context.SaveChanges();
            }
            
            string startMsg = addedNew == true ? "Added new" : "Updated";

            QueueMessage($"{startMsg} setting {settingKey}!");
        }
    }
}
