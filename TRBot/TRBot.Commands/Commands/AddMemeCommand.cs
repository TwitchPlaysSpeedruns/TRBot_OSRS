﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Consoles;
using TRBot.Parsing;
using TRBot.Permissions;
using TRBot.Data;
using TRBot.Logging;

namespace TRBot.Commands
{
    /// <summary>
    /// Adds a meme.
    /// </summary>
    public sealed class AddMemeCommand : BaseCommand
    {
        /// <summary>
        /// The max length for memes.
        /// </summary>
        public const int MAX_MEME_NAME_LENGTH = 50;

        private string UsageMessage = "Usage: \"memename (enclose in \" quotes for multi-word)\" \"memevalue\"";

        public AddMemeCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count < 1)
            {
                QueueMessage(UsageMessage);
                return;
            }

            string userName = args.Command.ChatMessage.Username;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = DataHelper.GetUserNoOpen(userName, context);

                if (user != null && user.HasEnabledAbility(PermissionConstants.ADD_MEME_ABILITY) == false)
                {
                    QueueMessage("You do not have the ability to add memes.");
                    return;
                }
            }

            string memeValue = string.Empty;
            string argsStr = args.Command.ArgumentsAsString;
            
            //TRBot.Logging.Logger.Information($"ArgsStr = {argsStr}");

            //Check if it's not a multi-word meme
            if (argsStr.StartsWith("\"", StringComparison.Ordinal) == false)
            {
                //Logger.Information("Meme name doesn't start with \"");

                ParseSingleWordMeme(argsStr);
                return;
            }

            //Find everything enclosed in quotes
            const string quoteRegex = "\".+\"";

            Match m = Regex.Match(argsStr, quoteRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            //Logger.Information($"Args Length = {argsStr.Length} | M Length = {m.Length}");

            //Ensure the match starts with the first character in the string
            if (m.Success == false || m.Index != 0)
            {
                //Logger.Information($"Success = {m.Success} | Index = {m.Index}");
                
                ParseSingleWordMeme(argsStr);
                return;
            }

            //"hi there" friend = length 10 of match
            //"hi there" friend = index 11 for start of meme value

            //No meme value
            if (argsStr.Length <= m.Length + 1)
            {
                ParseSingleWordMeme(argsStr);
                return;
            }

            //There's no whitespace after the match, so this isn't a proper multi-word meme
            if (char.IsWhiteSpace(argsStr[m.Length]) == false)
            {
                ParseSingleWordMeme(argsStr);
                return;
            }

            //Grab everything past the match as the meme value
            memeValue = argsStr.Substring(m.Length + 1);

            //Exclude the quotes in the meme name
            //Trim whitespace from the start and end of the meme name
            string memeName = m.Value.Substring(1, m.Length - 2).Trim();

            //Validate and add the multi-word meme
            ValidateAndAddMeme(memeName, memeValue);
        }

        private void ParseSingleWordMeme(string argsStr)
        {
            //Logger.Information($"{nameof(ParseSingleWordMeme)} with: {argsStr}");

            //Find the first whitespace
            int whiteSpaceIndex = argsStr.IndexOf(' ', 0);
            
            //"hi there = index 3 for whitespace

            if (whiteSpaceIndex < 0 || argsStr.Length <= (whiteSpaceIndex + 1))
            {
                QueueMessage("No meme value given.");
                return;
            }

            string memeName = argsStr.Substring(0, whiteSpaceIndex);
            string memeValue = argsStr.Substring(whiteSpaceIndex + 1);

            ValidateAndAddMeme(memeName, memeValue);
        }

        private void ValidateAndAddMeme(string memeName, string memeValue)
        {
            if (memeName[0] == '/' || memeValue[0] == '/')
            {
                QueueMessage("Memes cannot start with Twitch chat commands!");
                return;
            }

            if (memeName.StartsWith(InputMacroPreparser.DEFAULT_MACRO_START) == true)
            {
                QueueMessage($"Memes cannot start with \"{InputMacroPreparser.DEFAULT_MACRO_START}\".");
                return;
            }

            if (memeName[0] == DataConstants.COMMAND_IDENTIFIER)
            {
                QueueMessage($"Memes cannot start with \'{DataConstants.COMMAND_IDENTIFIER}\'.");
                return;
            }

            if (memeName.Length > MAX_MEME_NAME_LENGTH)
            {
                QueueMessage($"Memes may have up to a max of {MAX_MEME_NAME_LENGTH} characters in their name!");
                return;
            }

            string memeToLower = memeName.ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Meme meme = context.Memes.FirstOrDefault(m => m.MemeName == memeToLower);

                if (meme != null)
                {
                    meme.MemeValue = memeValue;

                    QueueMessage($"Meme \"{memeToLower}\" overwritten!");
                }
                else
                {
                    Meme newMeme = new Meme(memeToLower, memeValue);
                    context.Memes.Add(newMeme);

                    QueueMessage($"Added meme \"{memeToLower}\"!");
                }

                context.SaveChanges();
            }
        }
    }
}
