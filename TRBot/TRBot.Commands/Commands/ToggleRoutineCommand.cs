﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;
using TRBot.Routines;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that toggles a routine's enabled state.
    /// </summary>
    public sealed class ToggleRoutineCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"routine name\", (\"true\" or \"false\")";

        public ToggleRoutineCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with the incorrect number of arguments
            if (argCount != 2)
            {
                QueueMessage(UsageMessage);
                return;
            }

            string routineName = arguments[0].ToLowerInvariant();
            string enabledStr = arguments[1];
            bool routineEnabled = false;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Find the routine
                RoutineData routData = context.Routines.FirstOrDefault(r => r.Name == routineName);

                if (routData == null)
                {
                    QueueMessage($"Cannot find a routine named \"{routineName}\".");
                    return;
                }

                if (bool.TryParse(enabledStr, out routineEnabled) == false)
                {
                    QueueMessage("Incorrect routine enabled state specified.");
                    return;
                }

                routData.Enabled = (routineEnabled == true) ? 1L : 0L;

                context.SaveChanges();
            }

            BaseRoutine baseRoutine = RoutineHandler.FindRoutine(routineName);
            baseRoutine.Enabled = routineEnabled;

            QueueMessage($"Successfully set the enabled state of routine \"{routineName}\" to {routineEnabled}!");
        }
    }
}
