﻿using System;

namespace TRBot.Main
{
    /// <summary>
    /// Message queue data.
    /// </summary>
    public struct MsgQueueData
    {
        public MsgQueueTypes MsgQueueType; 
        public EventArgs EvtArgs;

        public MsgQueueData(in MsgQueueTypes msgQueueType, EventArgs evtArgs)
        {
            MsgQueueType = msgQueueType;
            EvtArgs = evtArgs;
        }

        public override bool Equals(object obj)
        {
            if (obj is MsgQueueData msgQueueData)
            {
                return (this == msgQueueData);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 41;
                hash = (hash * 31) + (int)MsgQueueType;
                hash = (hash * 31) + EvtArgs?.GetHashCode() ?? 0;
                return hash;
            }
        }

        public static bool operator ==(MsgQueueData a, MsgQueueData b)
        {
            return (a.MsgQueueType == b.MsgQueueType && a.EvtArgs == b.EvtArgs);
        }

        public static bool operator !=(MsgQueueData a, MsgQueueData b)
        {
            return !(a == b);
        }
    }
}
