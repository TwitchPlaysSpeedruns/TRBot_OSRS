﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TRBot.Parsing;
using TRBot.Connection;
using TRBot.Connection.Twitch;
using TRBot.Connection.WebSocket;
using TRBot.Consoles;
using TRBot.VirtualControllers;
using TRBot.Misc;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Commands;
using TRBot.Permissions;
using TRBot.Routines;
using TRBot.Logging;
using TRBot.Events;
using TRBot.WebSocket;
using TwitchLib.Client;
using TwitchLib.Client.Models;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Main
{
    public sealed class BotProgram : IDisposable
    {
        public bool Initialized { get; private set; } = false;

        private IClientService ClientService = null;

        private ICommandHandler CmdHandler = null;
        private DatabaseManager DatabaseMngr = null;

        private IBotMessageHandler MsgHandler = null;
        private IRoutineHandler RoutineHndler = null;
        private IDataReloader DataReloader = new DataReloader();
        private ITRBotLogger Logger = null;
        private IVirtualControllerContainer VControllerContainer = null;
        private IEventDispatcher EvtDispatcher = null;
        private IWebSocketManager WebSocketMngr = null;
        private IInputHandler InputHndlr = null;

        private CrashHandler crashHandler = null;

        //Store the function to reduce garbage, since this one is being called constantly
        private Func<Settings, bool> ThreadSleepFindFunc = null;

        #region Message Queue

        /// <summary>
        /// The message queue.
        /// </summary>
        private ConcurrentQueue<MsgQueueData> MsgQueue = new ConcurrentQueue<MsgQueueData>();

        private CancellationTokenSource CancelTokenSource = null;

        #endregion

        public BotProgram()
        {
            //Set up the logger
            string logPath = Path.Combine(LoggingConstants.LogFolderPath, LoggingConstants.LOG_FILE_NAME);
            if (Utilities.FileHelpers.ValidatePathForFile(logPath) == false)
            {
                Console.WriteLine("Logger path cannot be validated. This is a problem! Double check the path is correct.");
            }

            //Set up the logger
            //Cap the size at 10 MB
            Logger = new TRBotLogger(logPath, Serilog.Events.LogEventLevel.Verbose,
                Serilog.RollingInterval.Day, 1024L * 1024L * 10L, TimeSpan.FromSeconds(60d));

            //Set up crash handler
            crashHandler = new CrashHandler(Logger);

            //Set up other core dependencies
            InputHndlr = new InputHandler(Logger);
            ThreadSleepFindFunc = FindThreadSleepTime;

            //Call this to set the application start time
            DateTime start = Application.ApplicationStartTimeUTC;
        }

        //Clean up anything we need to here
        public void Dispose()
        {
            if (Initialized == false)
                return;

            StopQueueThread();

            UnsubscribeClientServiceEvents();

            ClientService?.CleanUp();

            MsgHandler?.CleanUp();
            RoutineHndler?.CleanUp();
            CmdHandler?.CleanUp();
            DataReloader?.Dispose();

            if (ClientService?.IsConnected == true)
                ClientService.Disconnect();

            //Dispose and relinquish the virtual controllers when we're done
            VControllerContainer?.VControllerMngr?.Dispose();

            Logger.Dispose();
        }

        //This initializes a whole lot, including the logger, database, virtual controllers, command handler, and routines
        //The order matters for some of these; notably, the database must be initialized before accessing any settings
        //If initialization fails somewhere important, log a error message and abort execution
        public void Initialize()
        {
            if (Initialized == true)
                return;

            //Use invariant culture
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            //Initialize database
            string databasePath = Path.Combine(DataConstants.DataFolderPath, DataConstants.DATABASE_FILE_NAME);

            Logger.Information($"Validating database at: {databasePath}");
            if (Utilities.FileHelpers.ValidatePathForFile(databasePath) == false)
            {
                Logger.Error($"Cannot create database path at {databasePath}. Check if you have permission to write to this directory. Aborting.");
                return;
            }

            Logger.Information("Database path validated! Initializing database and importing migrations.");

            DatabaseMngr = new DatabaseManager(databasePath, Logger);
            DatabaseMngr.InitAndMigrateContext();
            
            Logger.Information("Checking to initialize default values for missing database entries.");

            //Check for and initialize default values if the database was newly created or needs updating
            int addedDefaultEntries = DataHelper.InitDefaultData(DatabaseMngr, Logger);

            if (addedDefaultEntries > 0)
            {
                Logger.Information($"Added {addedDefaultEntries} additional entries to the database.");
            }

            //Set the logger's minimum log level
            long logLevel = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.LOG_LEVEL, (long)Serilog.Events.LogEventLevel.Information);
            Logger.MinLoggingLevel = (Serilog.Events.LogEventLevel)logLevel;

            Logger.Information("Initializing client service...");

            //Initialize client service
            InitClientService();

            //If the client service doesn't exist, we can't continue
            if (ClientService == null)
            {
                Logger.Error($"Client service failed to initialize; please check your {nameof(SettingsConstants.CLIENT_SERVICE_TYPE)} setting and service login settings. Aborting.");
                return;
            }

            Logger.Information("Initializing message handler...");

            InitMessageHandler();

            //Subscribe to events
            UnsubscribeClientServiceEvents();
            SubscribeClientServiceEvents();

            Logger.Information("Setting up virtual controller manager...");

            InitVControllerManager();

            int controllerCount = 0;

            //Clamp the controller count to the min and max allowed by the virtual controller manager
            using (BotDBContext context = DatabaseMngr.OpenContext())
            { 
                Settings joystickCountSetting = DataHelper.GetSettingNoOpen(SettingsConstants.JOYSTICK_COUNT, context);

                int minCount = VControllerContainer.VControllerMngr.MinControllers;
                int maxCount = VControllerContainer.VControllerMngr.MaxControllers;

                //Validate controller count
                if (joystickCountSetting.ValueInt < minCount)
                {
                    MsgHandler.QueueMessage($"Controller count of {joystickCountSetting.ValueInt} in database is invalid. Clamping to the min of {minCount}.");
                    joystickCountSetting.ValueInt = minCount;
                    context.SaveChanges();
                }
                else if (joystickCountSetting.ValueInt > maxCount)
                {
                    MsgHandler.QueueMessage($"Controller count of {joystickCountSetting.ValueInt} in database is invalid. Clamping to the max of {maxCount}.");
                    joystickCountSetting.ValueInt = maxCount;
                    context.SaveChanges();
                }

                controllerCount = (int)joystickCountSetting.ValueInt;
            }

            VControllerContainer.VControllerMngr.Initialize();
            int acquiredCount = VControllerContainer.VControllerMngr.InitControllers(controllerCount);

            Logger.Information($"Set up virtual controller manager {VControllerContainer.VControllerType} and acquired {acquiredCount} controllers!");

            Logger.Information($"Initializing WebSocket server!");

            InitWebSocketServer();

            Logger.Information($"Initializing event dispatcher!");

            InitEventDispatcher(WebSocketMngr);

            Logger.Information($"Initializing routine handler!");

            RoutineHndler = new RoutineHandler(DatabaseMngr, Logger, DataReloader, MsgHandler,
                VControllerContainer, EvtDispatcher, WebSocketMngr, InputHndlr, null);

            //Initialize routines
            InitRoutines();

            Logger.Information($"Initializing command handler!");

            CmdHandler = new CommandHandler(DatabaseMngr, Logger, DataReloader, RoutineHndler,
                MsgHandler, VControllerContainer, EvtDispatcher, WebSocketMngr, InputHndlr,
                new System.Reflection.Assembly[]
                { 
                    typeof(TRBot.Integrations.LiveSplitOne.LiveSplitOneSocketService).Assembly
                });

            CmdHandler.Initialize();

            DataReloader.SoftDataReloadedEvent -= OnSoftReload;
            DataReloader.SoftDataReloadedEvent += OnSoftReload;

            DataReloader.HardDataReloadedEvent -= OnHardReload;
            DataReloader.HardDataReloadedEvent += OnHardReload;

            Logger.Information($"Starting message queue thread...");

            //Start the message queue
            StartQueueThread();

            Logger.Information($"Started message queue thread!");

            Initialized = true;
        }

        public void Run()
        {
            if (ClientService.IsConnected == true)
            {
                Logger.Information("Client is already connected and running!");
                return;
            }

            try
            {
                ClientService.Connect();
            }
            catch (Exception e)
            {
                Logger.Error($"Unable to connect to client service: {e.Message}\n{e.StackTrace}");
            }

            //Run
            while (true)
            {
                //Store the bot's uptime
                DateTime utcNow = DateTime.UtcNow;

                //Update queued messages
                MsgHandler.Update(utcNow);

                //Update routines
                RoutineHndler.Update(utcNow);

                int threadSleep = 500;

                //Get the thread sleep value from the database
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    Settings threadSleepSetting = context.SettingCollection.AsNoTracking().FirstOrDefault(ThreadSleepFindFunc);

                    //Clamp to avoid an exception - the code that changes this should handle values below 0,
                    //but the database can manually be changed to have lower values 
                    threadSleep = Math.Clamp((int)threadSleepSetting.ValueInt, 0, int.MaxValue);
                }

                Thread.Sleep(threadSleep);
            }
        }

        private void UnsubscribeClientServiceEvents()
        {
            ClientService.EventHandler.UserSentMessageEvent -= OnUserSentMessage;
            ClientService.EventHandler.UserNewlySubscribedEvent -= OnNewSubscriber;
            ClientService.EventHandler.UserReSubscribedEvent -= OnReSubscriber;
            ClientService.EventHandler.WhisperReceivedEvent -= OnWhisperReceived;
            ClientService.EventHandler.ChatCommandReceivedEvent -= OnChatCommandReceived;
            ClientService.EventHandler.OnJoinedChannelEvent -= OnJoinedChannel;
            ClientService.EventHandler.ChannelHostedEvent -= OnBeingHosted;
            ClientService.EventHandler.OnConnectedEvent -= OnConnected;
            ClientService.EventHandler.OnConnectionErrorEvent -= OnConnectionError;
            ClientService.EventHandler.OnReconnectedEvent -= OnReconnected;
            ClientService.EventHandler.OnDisconnectedEvent -= OnDisconnected;
        }

        private void SubscribeClientServiceEvents()
        {
            ClientService.EventHandler.UserSentMessageEvent += OnUserSentMessage;
            ClientService.EventHandler.UserNewlySubscribedEvent += OnNewSubscriber;
            ClientService.EventHandler.UserReSubscribedEvent += OnReSubscriber;
            ClientService.EventHandler.WhisperReceivedEvent += OnWhisperReceived;
            ClientService.EventHandler.ChatCommandReceivedEvent += OnChatCommandReceived;
            ClientService.EventHandler.OnJoinedChannelEvent += OnJoinedChannel;
            ClientService.EventHandler.ChannelHostedEvent += OnBeingHosted;
            ClientService.EventHandler.OnConnectedEvent += OnConnected;
            ClientService.EventHandler.OnConnectionErrorEvent += OnConnectionError;
            ClientService.EventHandler.OnReconnectedEvent += OnReconnected;
            ClientService.EventHandler.OnDisconnectedEvent += OnDisconnected;
        }

        private void InitMessageHandler()
        {
            //Set client service and message cooldown
            MsgHandler = new BotMessageHandler(Logger, ClientService);

            MessageThrottlingOptions msgThrottleOption = (MessageThrottlingOptions)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.MESSAGE_THROTTLE_TYPE, 0L);
            long msgCooldown = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.MESSAGE_COOLDOWN, 30000L);
            long msgThrottleCount = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.MESSAGE_THROTTLE_COUNT, 20L);
            MsgHandler.SetMessageThrottling(msgThrottleOption, new MessageThrottleData(msgCooldown, msgThrottleCount));

            string messagePrefix = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.MESSAGE_PREFIX, string.Empty);
            MsgHandler.SetMessagePrefix(messagePrefix);

            ClientServiceTypes clientServiceType = DataHelper.GetClientServiceType(DatabaseMngr);
            if (clientServiceType == ClientServiceTypes.Terminal)
            {
                MsgHandler.SetLogToLogger(false);
            }
        }

        private void InitVControllerManager()
        {
            VirtualControllerTypes lastVControllerType = (VirtualControllerTypes)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.LAST_VCONTROLLER_TYPE, 0L);

            VirtualControllerTypes curVControllerType = VControllerHelper.ValidateVirtualControllerType(lastVControllerType, TRBotOSPlatform.CurrentOS);

            //Show a message saying the previous value wasn't supported and save the changes
            if (VControllerHelper.IsVControllerSupported(lastVControllerType, TRBotOSPlatform.CurrentOS) == false)
            {
                MsgHandler.QueueMessage($"Current virtual controller {lastVControllerType} is not supported by the {TRBotOSPlatform.CurrentOS} platform. Switched it to the default of {curVControllerType} for this platform.");                

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    Settings lastVControllerSetting = DataHelper.GetSettingNoOpen(SettingsConstants.LAST_VCONTROLLER_TYPE, context);
                    lastVControllerSetting.ValueInt = (long)curVControllerType;
                    context.SaveChanges();
                }
            }

            IVirtualControllerManager controllerMngr = VControllerHelper.GetVControllerMngrForType(curVControllerType, Logger);

            VControllerContainer = new VirtualControllerContainer(controllerMngr, curVControllerType);
        }

        private void InitRoutines()
        {
            RoutineHndler.Initialize();

            //Add the periodic input routine if it's enabled
            long periodicInputEnabled = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.PERIODIC_INPUT_ENABLED, 0L);
            if (periodicInputEnabled > 0L)
            {
                RoutineHndler.AddRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME, new PeriodicInputRoutine());
            }
        }

        private void InitWebSocketServer()
        {
            WebSocketMngr = new WebSocketManager(Logger);

            long webSocketServerEnabled = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.WEBSOCKET_SERVER_ENABLED, 0);

            if (webSocketServerEnabled > 0)
            {
                string webSocketAddress = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.WEBSOCKET_SERVER_ADDRESS, "ws://127.0.0.1:4350");

                bool started = WebSocketMngr.StartServer(webSocketAddress);

                if (started == false)
                {
                    Logger.Warning($"Failed to start WebSocket server. Ensure that {SettingsConstants.WEBSOCKET_SERVER_ADDRESS} is a valid WebSocket address.");
                }
                else
                {
                    Logger.Information($"Started WebSocket server at \"{WebSocketMngr.ServerUrl}\" on port {WebSocketMngr.ServerPort}.");
                }
            }
        }

        private void InitEventDispatcher(IWebSocketManager webSocketManager)
        {
            long evtEnabled = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.EVENT_DISPATCHER_ENABLED, 0);
            string evtPath = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.EVENT_DISPATCHER_PATH, "/evt");

            EvtDispatcher = new WebSocketEventDispatcher(evtPath, webSocketManager, Logger);

            if (evtEnabled <= 0)
            {
                EvtDispatcher.DisableDispatcher();
            }
        }

#region Events

        private void OnConnected(EvtConnectedArgs e)
        {
            Logger.Information($"Bot connected!");
        }

        private void OnConnectionError(EvtConnectionErrorArgs e)
        {
            MsgHandler.QueueMessage($"Failed to connect: {e.Error.Message}");
        }

        private void OnJoinedChannel(EvtJoinedChannelArgs e)
        {
            Logger.Information($"Joined channel \"{e.Channel}\"");

            MsgHandler.SetChannelName(e.Channel);

            string connectMessage = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.CONNECT_MESSAGE, "Connected!");
            MsgHandler.QueueMessage(connectMessage);
        }

        private void OnChatCommandReceived(EvtChatCommandArgs e)
        {
            //TRBotLogger.Information($"Sent command: {e.Command.ChatMessage.Message}");

            AddMessageToQueue(new MsgQueueData(MsgQueueTypes.Command, e));
        }

        private void OnUserSentMessage(EvtUserMessageArgs e)
        {
            //TRBotLogger.Information($"Sent message: {e.UsrMessage.Message}"); 

            //TRBotLogger.Information($"Received message from {e.UsrMessage.Username} at {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.ffff")}");

            AddMessageToQueue(new MsgQueueData(MsgQueueTypes.Message, e));
        }

        private void HandleChatCommandReceived(EvtChatCommandArgs e)
        {
            //MsgHandler.QueueMessage($"Received command \"{e.Command.CommandText}\"");

            try
            {
                CmdHandler.HandleCommand(e);
            }
            catch (Exception exc)
            {
                Logger.Error($"Ran into exception on command {e.Command.CommandText}: {exc.Message}\n{exc.StackTrace}"); 
            }
        }

        private void HandleUserSentMessage(EvtUserMessageArgs e)
        {
            //TRBotLogger.Information($"Arrived at {nameof(HandleUserSentMessage)}");

            //Look for a user with this name
            string userName = e.UsrMessage.Username;
            long userLevel = 0L;

            if (string.IsNullOrEmpty(userName) == false)
            {
                User user = DataHelper.GetOrAddUser(DatabaseMngr, userName, out bool added);
                
                if (added == true)
                {
                    //Get the new user message and replace the variable with their name
                    string newUserMessage = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.NEW_USER_MESSAGE, $"Welcome to the stream, {userName}!");
                    newUserMessage = newUserMessage.Replace("{0}", userName);
                    
                    MsgHandler.QueueMessage(newUserMessage);
                }
                
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    user = DataHelper.GetUserNoOpen(userName, context);
                    
                    //Increment message count and save
                    if (user != null && user.IsOptedOut == false)
                    {
                        user.Stats.TotalMessageCount++;
                        context.SaveChanges();
                    }

                    //Check for memes if the user isn't ignoring them
                    if (user != null && user.Stats.IgnoreMemes == 0)
                    {
                        string possibleMeme = e.UsrMessage.Message.ToLower();
                        Meme meme = context.Memes.AsNoTracking().FirstOrDefault((m) => m.MemeName == possibleMeme);
                        if (meme != null)
                        {
                            MsgHandler.QueueMessage(meme.MemeValue);
                        }
                    }

                    if (user != null)
                    {
                        userLevel = user.Level;
                    }
                }
            }

            //TRBotLogger.Information($"Processing message at {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.ffff")}");

            EvtDispatcher.DispatchEvent(EventTypes.MESSAGE,
                new MessageEventData(e.UsrMessage.DisplayName, userLevel, e.UsrMessage.Message));

            bool isValidInput = ProcessMsgAsInput(e);

            //Add only non-inputs to simulate data
            if (isValidInput == true)
            {
                return;
            }

            //Add the message to the user's simulate data
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                string userNameLowered = userName?.ToLowerInvariant();

                User user = context.Users.Include(u => u.Stats).FirstOrDefault(u => u.Name == userNameLowered);

                if (user == null)
                {
                    return;
                }

                bool isCommand = e.UsrMessage.Message.StartsWith(DataConstants.COMMAND_IDENTIFIER);
                bool isNullOrWhitespace = string.IsNullOrWhiteSpace(e.UsrMessage.Message);

                //Handle simulate data for the user if the message doesn't start with a command
                //Also ensure the user is opted into both bot stats and simulate data
                if (isCommand == false && isNullOrWhitespace == false
                    && user.IsOptedOut == false && user.IsOptedIntoSimulate == true)
                {
                    string simulateHistory = user.Stats.SimulateHistory;

                    //Replace all whitespace with space (' ') for consistency
                    string msgWhitespace = Helpers.ReplaceAllWhitespaceWithSpace(e.UsrMessage.Message);

                    //No history - set to a default value
                    if (string.IsNullOrEmpty(simulateHistory) == true)
                    {
                        simulateHistory = string.Empty;
                    }

                    int maxLength = (int)DataHelper.GetSettingIntNoOpen(SettingsConstants.MAX_USER_SIMULATE_STRING_LENGTH, context, 10000L);

                    //Append a space followed by the user's message
                    if (simulateHistory.Length > 0)
                    {
                        simulateHistory += " " + msgWhitespace;
                    }
                    //Ignore the space if the history is empty
                    else
                    {
                        simulateHistory = msgWhitespace;
                    }

                    int diff = simulateHistory.Length - maxLength;

                    //If we're over the max length, trim the difference from the start of the history
                    if (diff > 0)
                    {
                        simulateHistory = simulateHistory.Remove(0, diff);
                    }

                    //Set and save
                    user.Stats.SimulateHistory = simulateHistory;

                    context.SaveChanges();
                }
            }
        }

        private void OnWhisperReceived(EvtWhisperMessageArgs e)
        {
            
        }

        private void OnBeingHosted(EvtOnHostedArgs e)
        {
            string hostedMessage = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.BEING_HOSTED_MESSAGE, string.Empty);

            if (string.IsNullOrEmpty(hostedMessage) == false)
            {
                string finalMsg = hostedMessage.Replace("{0}", e.HostedData.HostedByChannel);
                MsgHandler.QueueMessage(finalMsg);
            }
        }

        private void OnNewSubscriber(EvtOnSubscriptionArgs e)
        {
            string newSubscriberMessage = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.NEW_SUBSCRIBER_MESSAGE, string.Empty);

            if (string.IsNullOrEmpty(newSubscriberMessage) == false)
            {
                string finalMsg = newSubscriberMessage.Replace("{0}", e.SubscriptionData.DisplayName);
                MsgHandler.QueueMessage(finalMsg);
            }
        }

        private void OnReSubscriber(EvtOnReSubscriptionArgs e)
        {
            string resubscriberMessage = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.RESUBSCRIBER_MESSAGE, string.Empty);

            if (string.IsNullOrEmpty(resubscriberMessage) == false)
            {
                string finalMsg = resubscriberMessage.Replace("{0}", e.ReSubscriptionData.DisplayName).Replace("{1}", e.ReSubscriptionData.Months.ToString());
                MsgHandler.QueueMessage(finalMsg);
            }
        }

        private void OnReconnected(EvtReconnectedArgs e)
        {
            string reconnectedMessage = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.RECONNECTED_MESSAGE, string.Empty);

            if (string.IsNullOrEmpty(reconnectedMessage) == false)
            {
                MsgHandler.QueueMessage(reconnectedMessage);
            }
        }

        private void OnDisconnected(EvtDisconnectedArgs e)
        {
            Logger.Warning("Bot disconnected! Please check your internet connection.");
        }

        private bool ProcessMsgAsInput(EvtUserMessageArgs e)
        {
            //Ignore commands as inputs
            if (e.UsrMessage.Message.StartsWith(DataConstants.COMMAND_IDENTIFIER) == true)
            {
                return false;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                string userNameLowered = e.UsrMessage.Username?.ToLowerInvariant();

                User user = context.Users.AsNoTracking().Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility).FirstOrDefault(u => u.Name == userNameLowered);

                //Check if the user is silenced and ignore the message if so
                if (user != null && user.HasEnabledAbility(PermissionConstants.SILENCED_ABILITY) == true)
                {
                    return false;
                }
            }

            GameConsole usedConsole = null;

            int lastConsoleID = 1;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                lastConsoleID = (int)DataHelper.GetSettingIntNoOpen(SettingsConstants.LAST_CONSOLE, context, 1L);
                GameConsole lastConsole = context.Consoles.AsNoTracking().Include(c => c.InputList)
                    .Include(c => c.InvalidCombos).ThenInclude(ic => ic.Input).FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidCombos);
                }
            }

            //If there are no valid inputs, don't attempt to parse
            if (usedConsole == null)
            {
                MsgHandler.QueueMessage($"The current console does not point to valid data. Please set a different console to use, or if none are available, add one.");
                return false;
            }

            if (usedConsole.ConsoleInputs.Count == 0)
            {
                MsgHandler.QueueMessage($"The current console, \"{usedConsole.Name}\", does not have any available inputs.");
            }

            ParsedInputSequence inputSequence = default;
            string userName = e.UsrMessage.Username;
            int defaultDur = 200;
            int defaultPort = 0;
            long userLevel = 0L;
            Dictionary<string, int> userRestrictedInputs = null;

            try
            {
                int maxDur = 60000;
                
                //Get default and max input durations
                //Use user overrides if they exist, otherwise use the global values
                User user = null;
                
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    if (string.IsNullOrEmpty(userName) == false)
                    {
                        string userNameLowered = userName.ToLowerInvariant();
                        user = context.Users.AsNoTracking().Include(u => u.RestrictedInputs)
                            .ThenInclude(ri => ri.inputData).FirstOrDefault(u => u.Name == userNameLowered);

                        if (user != null)
                        {
                            userRestrictedInputs = user.GetRestrictedInputs();
                        }
                    }
                }

                if (user != null)
                {
                    //Get default controller port
                    defaultPort = (int)user.ControllerPort;
                    
                    //Get level
                    userLevel = user.Level;
                }

                defaultDur = (int)DataHelper.GetUserOrGlobalDefaultInputDur(DatabaseMngr, userName);
                maxDur = (int)DataHelper.GetUserOrGlobalMaxInputDur(DatabaseMngr, userName);

                //TRBotLogger.Information($"Default dur: {defaultDur} | Max dur: {maxDur}");

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    //Get input synonyms for this console
                    IQueryable<InputSynonym> synonyms = context.InputSynonyms.Where(syn => syn.ConsoleID == lastConsoleID);

                    StandardParserValidator validator = new StandardParserValidator(userRestrictedInputs,
                        userLevel, usedConsole, VControllerContainer.VControllerMngr, Logger);

                    //TRBotLogger.Information($"STARTED parsing and validation at {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.ffff")}");

                    StandardParser standardParser = StandardParser.CreateStandard(context.Macros, synonyms,
                        usedConsole.GetInputNames(), defaultPort, VControllerContainer.VControllerMngr.ControllerCount - 1,
                        defaultDur, maxDur, true, validator);

                    //TRBotLogger.Information($"ENDED parsing and validation at {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.ffff")}");

                    //Parse inputs to get our parsed input sequence
                    inputSequence = standardParser.ParseInputs(e.UsrMessage.Message);
                }

                //TRBotLogger.Debug(inputSequence.ToString());
                //TRBotLogger.Debug("Reverse Parsed (on parse): " + ReverseParser.ReverseParse(inputSequence, usedConsole,
                //    new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowNonDefaultPorts, defaultPort,
                //    ReverseParser.ShowDurationTypes.ShowNonDefaultDurations, defaultDur)));
            }
            catch (Exception exception)
            {
                string excMsg = exception.Message;

                //Handle parsing exceptions
                MsgHandler.QueueMessage($"ERROR PARSING: {excMsg} | {exception.StackTrace}", Serilog.Events.LogEventLevel.Warning);
                inputSequence.ParsedInputResult = ParsedInputResults.Invalid;
            }

            //Check for non-valid messages
            if (inputSequence.ParsedInputResult != ParsedInputResults.Valid)
            {
                //Display error message for invalid inputs
                if (inputSequence.ParsedInputResult == ParsedInputResults.Invalid)
                {
                    MsgHandler.QueueMessage(inputSequence.Error);
                }

                return false;
            }

            #region Parser Post-Process Validation
            
            /* All this validation may be able to be performed faster.
             * Find a way to speed it up.
             */

            long globalInputPermLevel = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.GLOBAL_INPUT_LEVEL, 0L);
            int userControllerPort = 0;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = DataHelper.GetUserNoOpen(e.UsrMessage.Username, context);
            
                //Ignore based on user level and permissions
                if (user != null && user.Level < globalInputPermLevel)
                {
                    MsgHandler.QueueMessage($"Inputs are restricted to levels {(PermissionLevels)globalInputPermLevel} and above.");
                    return false;
                }

                if (user != null)
                {
                    userControllerPort = (int)user.ControllerPort;
                }
            }

            //Add delays between inputs if we should
            if (DataHelper.GetUserOrGlobalMidInputDelay(DatabaseMngr, e.UsrMessage.Username, out long midInputDelay) == true)
            {
                //Get a blank input the user can perform
                InputData blankInpData = usedConsole.GetAvailableBlankInput(userLevel, userRestrictedInputs);

                if (blankInpData != null)
                {
                    //TRBotLogger.Information($"Chose {blankInpData.Name} as the blank input");

                    MidInputDelayData midInputDelayData = ParserPostProcess.InsertMidInputDelays(inputSequence,
                        userControllerPort, (int)midInputDelay, usedConsole, blankInpData);

                    //If it's successful, replace the input list and duration
                    if (midInputDelayData.Success == true)
                    {
                        int oldDur = inputSequence.TotalDuration;
                        inputSequence.Inputs = midInputDelayData.NewInputs;
                        inputSequence.TotalDuration = midInputDelayData.NewTotalDuration;

                        //TRBotLogger.Debug($"Mid input delay success. Message: {midInputDelayData.Message} | OldDur: {oldDur} | NewDur: {inputSequence.TotalDuration}\n{ReverseParser.ReverseParse(inputSequence, usedConsole, new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowAllPorts, 0))}");
                    }
                }
            }

            #endregion

            //Make sure inputs aren't stopped
            if (InputHndlr.InputsHalted == true)
            {
                //We can't process inputs because they're currently stopped
                MsgHandler.QueueMessage("New inputs cannot be processed until all other inputs have stopped.", Serilog.Events.LogEventLevel.Warning);
                return true;
            }

            //Fetch these values ahead of time to avoid passing the database context through so many methods    
            long autoPromoteEnabled = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.AUTO_PROMOTE_ENABLED, 0L);
            long autoPromoteInputReq = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.AUTO_PROMOTE_INPUT_REQ, long.MaxValue);
            long autoPromoteLevel = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.AUTO_PROMOTE_LEVEL, -1L);
            string autoPromoteMsg = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.AUTOPROMOTE_MESSAGE, string.Empty);

            bool addedInputCount = false;
            long prevInputCount = 0L;

            //TRBotLogger.Debug($"Reverse Parsed (post-process): " + ReverseParser.ReverseParse(inputSequence, usedConsole,
            //        new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowNonDefaultPorts, defaultPort,
            //        ReverseParser.ShowDurationTypes.ShowNonDefaultDurations, defaultDur)));

            //Get the max recorded inputs per-user
            long maxUserRecInps = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.MAX_USER_RECENT_INPUTS, 0L);

            //It's a valid input - save it in the user's stats
            //Also record the input if we should
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                string userNameLowered = userName?.ToLowerInvariant();

                User user = context.Users.Include(u => u.Stats).Include(u => u.RecentInputs).FirstOrDefault(u => u.Name == userNameLowered);

                //Ignore if the user is opted out
                if (user != null && user.IsOptedOut == false)
                {
                    prevInputCount = user.Stats.ValidInputCount;

                    user.Stats.ValidInputCount++;
                    addedInputCount = true;

                    context.SaveChanges();

                    //If we should store recent user inputs, do so
                    if (maxUserRecInps > 0)
                    {
                        //TRBotLogger.Information($"STARTED RECORDING input at {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.ffff")}");

                        //Get the input sequence - we may have added mid input delays between
                        //As a result, we'll need to reverse parse it
                        string message = ReverseParser.ReverseParse(inputSequence, usedConsole,
                            new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowNonDefaultPorts, (int)user.ControllerPort,
                            ReverseParser.ShowDurationTypes.ShowNonDefaultDurations, defaultDur));

                        //Add the recorded input
                        user.RecentInputs.Add(new RecentInput(message));
                        context.SaveChanges();

                        //TRBotLogger.Information($"ENDED RECORDING input at {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.ffff")}");

                        int diff = user.RecentInputs.Count - (int)maxUserRecInps;

                        //If we're over the max after adding, remove
                        if (diff > 0)
                        {
                            //Order by ascending ID and take the difference
                            //Lower IDs = older entries
                            IEnumerable<RecentInput> shouldRemove = user.RecentInputs.OrderBy(r => r.UserID).Take(diff);

                            foreach (RecentInput rec in shouldRemove)
                            {
                                user.RecentInputs.Remove(rec);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }

            bool autoPromoted = false;

            //Check if auto promote is enabled and auto promote the user if applicable
            if (addedInputCount == true)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    string userNameLowered = userName?.ToLowerInvariant();

                    User user = context.Users.Include(u => u.Stats).FirstOrDefault(u => u.Name == userNameLowered);
                    
                    //Check if the user was already autopromoted, autopromote is enabled,
                    //and if the user reached the autopromote input count requirement
                    if (user != null && user.Stats.AutoPromoted == 0 && autoPromoteEnabled > 0
                        && user.Stats.ValidInputCount >= autoPromoteInputReq)
                    {
                        //Only autopromote if this is a valid permission level
                        //We may not want to log or send a message for this, as it has potential to be very spammy,
                        //and it's not something the users can control
                        if (PermissionHelpers.IsValidPermissionValue(autoPromoteLevel) == true)
                        {
                            //Mark the user as autopromoted and save
                            user.Stats.AutoPromoted = 1;
                            autoPromoted = true;

                            context.SaveChanges();
                        }
                    }
                }

                //Send a message when a user ranks up
                string rankUpMsg = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.RANK_UP_MESSAGE, string.Empty);

                if (string.IsNullOrEmpty(rankUpMsg) == false)
                {
                    DisplayRank prevRank = DataHelper.GetUserDisplayRankAtValue(DatabaseMngr, prevInputCount);
                    DisplayRank newRank = DataHelper.GetUserDisplayRankAtValue(DatabaseMngr, prevInputCount + 1);

                    if (prevRank != null && newRank != null && prevRank.Rank != newRank.Rank)
                    {
                        string rankUpMsgFormatted = rankUpMsg;
                        rankUpMsgFormatted = rankUpMsgFormatted.Replace("{0}", userName)
                            .Replace("{1}", newRank.Rank.ToString())
                            .Replace("{2}", newRank.Label.ToString());

                        MsgHandler.QueueMessage(rankUpMsgFormatted);
                    }
                }
            }

            if (autoPromoted == true)
            {
                //If the user is already at or above this level, don't set them to it
                //Only set if the user is below
                if (userLevel < autoPromoteLevel)
                {
                    //Adjust abilities and promote to the new level
                    DataHelper.AdjustUserLvlAndAbilitiesOnLevel(DatabaseMngr, userName, autoPromoteLevel);

                    if (string.IsNullOrEmpty(autoPromoteMsg) == false)
                    {
                        PermissionLevels permLvl = (PermissionLevels)autoPromoteLevel;
                        string finalMsg = autoPromoteMsg.Replace("{0}", userName).Replace("{1}", permLvl.ToString());
                        MsgHandler.QueueMessage(finalMsg);
                    } 
                }
            }

            EvtDispatcher.DispatchEvent(EventTypes.INPUT,
                new InputEventData(e.UsrMessage.DisplayName, userLevel, e.UsrMessage.Message));

            InputModes inputMode = (InputModes)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.INPUT_MODE, 0L);

            //If the mode is Democracy, add it as a vote for this input
            if (inputMode == InputModes.Democracy)
            {
                //Set up the routine if it doesn't exist
                BaseRoutine foundRoutine = RoutineHndler.FindRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME);
                DemocracyRoutine democracyRoutine = null;

                if (foundRoutine == null)
                {
                    long voteTime = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.DEMOCRACY_VOTE_TIME, 10000L);

                    democracyRoutine = new DemocracyRoutine(voteTime);
                    RoutineHndler.AddRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME, democracyRoutine);
                }
                else
                {
                    democracyRoutine = (DemocracyRoutine)foundRoutine;
                }

                democracyRoutine.AddInputSequence(userName, e.UsrMessage.Message, inputSequence.Inputs);
            }
            //If it's Anarchy, carry out the input
            else
            {
                /************************************
                * Finally carry out the inputs now! *
                ************************************/

                //TRBotLogger.Information($"Carried out input at {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.ffff")}");

                InputHndlr.CarryOutInput(inputSequence.Inputs, usedConsole, VControllerContainer.VControllerMngr);
            }

            return true;
        }

#endregion

#region Message Queue Methods

        private void AddMessageToQueue(in MsgQueueData msgQueueData)
        {
            MsgQueue.Enqueue(msgQueueData);
        }

        private void StartQueueThread()
        {
            if (CancelTokenSource != null)
            {
                return;
            }

            CancelTokenSource = new CancellationTokenSource();

            Task.Run(() => QueueThread(CancelTokenSource.Token));
        }

        private void StopQueueThread()
        {
            if (CancelTokenSource == null || CancelTokenSource.IsCancellationRequested == true)
            {
                return;
            }

            CancelTokenSource.Cancel();
            CancelTokenSource.Dispose();
            CancelTokenSource = null;
        }

        public async Task QueueThread(CancellationToken cancelToken)
        {
            while (true)
            {
                await Task.Delay(1);
                
                //End if cancellation was requested
                if (cancelToken.IsCancellationRequested == true)
                {
                    break;
                }

                if (MsgQueue.Count == 0)
                {
                    continue;
                }

                bool dequeued = MsgQueue.TryDequeue(out MsgQueueData msgQueueData);

                if (dequeued == false)
                {
                    continue;
                }

                try
                {
                    switch (msgQueueData.MsgQueueType)
                    {
                        case MsgQueueTypes.Message:
                            HandleUserSentMessage(msgQueueData.EvtArgs as EvtUserMessageArgs);
                            break;
                        case MsgQueueTypes.Command:
                            HandleChatCommandReceived(msgQueueData.EvtArgs as EvtChatCommandArgs);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Logger.Error($"ERROR in {nameof(MsgQueue)} with type \"{msgQueueData.MsgQueueType}\" - {e.Message}\n{e.StackTrace}");
                    continue;
                }
            }
        }

#endregion

        private void InitClientService()
        {
            ClientServiceTypes clientServiceType = ClientServiceTypes.Terminal;
            using (BotDBContext dbContext = DatabaseMngr.OpenContext())
            {
                Settings setting = dbContext.SettingCollection.AsNoTracking().FirstOrDefault((set) => set.Key == SettingsConstants.CLIENT_SERVICE_TYPE);

                if (setting != null)
                {
                    clientServiceType = (ClientServiceTypes)setting.ValueInt;
                }
                else
                {
                    Logger.Warning($"Database does not contain the {SettingsConstants.CLIENT_SERVICE_TYPE} setting. It will default to {clientServiceType}.");
                }
            }

            Logger.Information($"Setting up client service: {clientServiceType}");

            if (clientServiceType == ClientServiceTypes.Terminal)
            {
                ClientService = new TerminalClientService(DataConstants.COMMAND_IDENTIFIER, Logger);
            }
            else if (clientServiceType == ClientServiceTypes.Twitch)
            {
                TwitchLoginSettings twitchSettings = ConnectionHelper.ValidateTwitchCredentialsPresent(DataConstants.DataFolderPath,
                    TwitchConstants.LOGIN_SETTINGS_FILENAME);

                //If either of these fields are empty, the data is invalid
                if (string.IsNullOrEmpty(twitchSettings.ChannelName) || string.IsNullOrEmpty(twitchSettings.Password)
                    || string.IsNullOrEmpty(twitchSettings.BotName))
                {
                    Logger.Error($"Twitch login settings are invalid. Please modify the data in the \"{TwitchConstants.LOGIN_SETTINGS_FILENAME}\" file.");
                    return;
                }

                TwitchClient client = new TwitchClient();
                ConnectionCredentials credentials = ConnectionHelper.MakeCredentialsFromTwitchLogin(twitchSettings);
                ClientService = new TwitchClientService(credentials, twitchSettings.ChannelName, DataConstants.COMMAND_IDENTIFIER, DataConstants.COMMAND_IDENTIFIER, true, Logger);
            }
            else if (clientServiceType == ClientServiceTypes.WebSocket)
            {
                WebSocketConnectSettings wsConnectSettings = ConnectionHelper.ValidateWebSocketCredentialsPresent(DataConstants.DataFolderPath,
                    WebSocketConstants.CONNECTION_SETTINGS_FILENAME);

                //If the connection field is empty, the data is invalid
                if (string.IsNullOrEmpty(wsConnectSettings.ConnectURL) == true)
                {
                    Logger.Error($"WebSocket connection settings are invalid; there's no given URL to connect to. Please modify the data in the \"{WebSocketConstants.CONNECTION_SETTINGS_FILENAME}\" file.");
                    return;
                }

                string connectURL = wsConnectSettings.ConnectURL;

                //The connection URL must start with the WebSocket protocol
                if (connectURL.StartsWith(WebSocketConstants.WEBSOCKET_PROTOCOL, StringComparison.Ordinal) == false
                    && connectURL.StartsWith(WebSocketConstants.WEBSOCKET_SECURE_PROTOCOL, StringComparison.Ordinal) == false)
                {
                    Logger.Error($"WebSocket connection URL does not start with \"{WebSocketConstants.WEBSOCKET_PROTOCOL}\" or \"{WebSocketConstants.WEBSOCKET_SECURE_PROTOCOL}\".");
                    return;
                }

                ClientService = new WebSocketClientService(wsConnectSettings.ConnectURL, DataConstants.COMMAND_IDENTIFIER, wsConnectSettings.BotName, Logger);
            }

            //Initialize service
            ClientService?.Initialize();
        }

        private bool FindThreadSleepTime(Settings setting)
        {
            return (setting.Key == SettingsConstants.MAIN_THREAD_SLEEP);
        }

        private void OnSoftReload()
        {
            HandleReloadBoth();
        }

        private void OnHardReload()
        {
            HandleReloadBoth();
        }

        private void HandleReloadBoth()
        {
            //Check for changes in the log level
            Serilog.Events.LogEventLevel logLevel = (Serilog.Events.LogEventLevel)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.LOG_LEVEL, (long)Serilog.Events.LogEventLevel.Information);
            if (logLevel != Logger.MinLoggingLevel)
            {
                Logger.Information($"Detected change in logging level - changing the logging level from {Logger.MinLoggingLevel} to {logLevel}");
                Logger.MinLoggingLevel = (logLevel);
            }

            //Check if the periodic input value changed, and enable/disable the routine if we should
            long periodicEnabled = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.PERIODIC_INPUT_ENABLED, 0L);
            if (periodicEnabled == 0)
            {
                //Remove the routine
                RoutineHndler.RemoveRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME);
            }
            else
            {
                BaseRoutine periodicInputRoutine = RoutineHndler.FindRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME);

                //Add the routine if it doesn't exist
                if (periodicInputRoutine == null)
                {
                    RoutineHndler.AddRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME, new PeriodicInputRoutine()); 
                }
            }

            //Check if the virtual controller type was changed
            if (LastVControllerTypeChanged() == true)
            {
                VirtualControllerTypes lastVControllerType = (VirtualControllerTypes)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.LAST_VCONTROLLER_TYPE, 0L);
                VirtualControllerTypes supportedVCType = VControllerHelper.ValidateVirtualControllerType(lastVControllerType, TRBotOSPlatform.CurrentOS);

                //Show a message saying the previous value wasn't supported
                if (VControllerHelper.IsVControllerSupported(lastVControllerType, TRBotOSPlatform.CurrentOS) == false)
                {
                    MsgHandler.QueueMessage($"Current virtual controller {lastVControllerType} is not supported by the {TRBotOSPlatform.CurrentOS} platform. Switched it to the default of {supportedVCType} for this platform.");                
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        Settings lastVControllerSetting = DataHelper.GetSettingNoOpen(SettingsConstants.LAST_VCONTROLLER_TYPE, context);
                        lastVControllerSetting.ValueInt = (long)supportedVCType;
                        context.SaveChanges();
                    }
                }

                ChangeVControllerType(supportedVCType);
            }
            else
            {
                ReinitVControllerCount();
            }

            //Handle message throttling changes
            MessageThrottlingOptions msgThrottle = (MessageThrottlingOptions)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.MESSAGE_THROTTLE_TYPE, 0L);
            long msgTime = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.MESSAGE_COOLDOWN, 30000L);
            long msgThrottleCount = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.MESSAGE_THROTTLE_COUNT, 20L);

            if (msgThrottle != MsgHandler.CurThrottleOption)
            {
                Logger.Information("Detected change in message throttling type - changing message throttler.");
            }

            MsgHandler.SetMessageThrottling(msgThrottle, new MessageThrottleData(msgTime, msgThrottleCount));

            //Handle message prefix changes
            string msgPrefix = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.MESSAGE_PREFIX, string.Empty);
            if (msgPrefix != MsgHandler.MessagePrefix)
            {
                Logger.Information("Detected change in message prefix - changing message prefix.");
            }

            MsgHandler.SetMessagePrefix(msgPrefix);

            //Handle WebSocket server changes
            long webSocketServerEnabled = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.WEBSOCKET_SERVER_ENABLED, 0L);
            if (webSocketServerEnabled <= 0)
            {
                WebSocketMngr.StopServer();
            }
            else
            {
                string webSocketServerAddress = DataHelper.GetSettingString(DatabaseMngr, SettingsConstants.WEBSOCKET_SERVER_ADDRESS, "ws://127.0.0.1:4350");

                WebSocketMngr.StartServer(webSocketServerAddress);
            }

            //Handle event dispatcher changes
            long evtDispatcherEnabled = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.EVENT_DISPATCHER_ENABLED, 0);
            
            if (evtDispatcherEnabled <= 0 && EvtDispatcher.Enabled == true)
            {
                EvtDispatcher.DisableDispatcher();
            }
            else if (evtDispatcherEnabled > 0 && EvtDispatcher.Enabled == false)
            {
                EvtDispatcher.EnableDispatcher();
            }
        }

        private bool LastVControllerTypeChanged()
        {
            VirtualControllerTypes vContType = (VirtualControllerTypes)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.LAST_VCONTROLLER_TYPE, 0L);

            return (vContType != VControllerContainer.VControllerType);
        }

        private void ChangeVControllerType(in VirtualControllerTypes newVControllerType)
        {
            MsgHandler.QueueMessage("Found virtual controller manager change in database. Stopping all inputs and reinitializing virtual controllers.");

            //Fetch the new controller manager
            IVirtualControllerManager controllerMngr = VControllerHelper.GetVControllerMngrForType(newVControllerType, Logger);

            int prevJoystickCount = (int)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.JOYSTICK_COUNT, 1L);
            int newJoystickCount = prevJoystickCount;

            //First, stop all inputs
            Task t = InputHndlr.StopAndHaltAllInputs();
            t.Wait();

            //Dispose the controller manager
            VControllerContainer.VControllerMngr.Dispose();

            VControllerContainer.SetVirtualControllerType(newVControllerType);
            VControllerContainer.SetVirtualControllerManager(controllerMngr);

            VControllerContainer.VControllerMngr.Initialize();

            //Validate virtual controller count for this virtual controller manager
            int minCount = VControllerContainer.VControllerMngr.MinControllers;
            int maxCount = VControllerContainer.VControllerMngr.MaxControllers;

            if (prevJoystickCount < minCount)
            {
                MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the min of {minCount}.");
                newJoystickCount = minCount;
            }
            else if (prevJoystickCount > maxCount)
            {
                MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the max of {maxCount}.");
                newJoystickCount = maxCount;
            }

            if (prevJoystickCount != newJoystickCount)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //Adjust the value and save
                    Settings joystickCountSetting = DataHelper.GetSettingNoOpen(SettingsConstants.JOYSTICK_COUNT, context);
                    joystickCountSetting.ValueInt = newJoystickCount;
                    
                    context.SaveChanges();
                }
            }

            int acquiredCount = VControllerContainer.VControllerMngr.InitControllers(newJoystickCount);

            //Resume inputs
            InputHndlr.ResumeRunningInputs();

            MsgHandler.QueueMessage($"Changed to virtual controller {VControllerContainer.VControllerType} and acquired {acquiredCount} controllers!");
        }

        private void ReinitVControllerCount()
        {
            int curVControllerCount = VControllerContainer.VControllerMngr.ControllerCount;

            int prevJoystickCount = (int)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.JOYSTICK_COUNT, 1L);
            int newJoystickCount = prevJoystickCount;

            int minCount = VControllerContainer.VControllerMngr.MinControllers;
            int maxCount = VControllerContainer.VControllerMngr.MaxControllers;

            //Validate controller count
            if (prevJoystickCount < minCount)
            {
                MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the min of {minCount}.");
                newJoystickCount = minCount;
            }
            else if (prevJoystickCount > maxCount)
            {
                MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the max of {maxCount}.");
                newJoystickCount = maxCount;
            }

            if (prevJoystickCount != newJoystickCount)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //Adjust the value and save
                    Settings joystickCountSetting = DataHelper.GetSettingNoOpen(SettingsConstants.JOYSTICK_COUNT, context);
                    joystickCountSetting.ValueInt = newJoystickCount;

                    context.SaveChanges();
                }
            }

            //Same count, so ignore
            if (curVControllerCount == newJoystickCount)
            {
                return;
            }

            MsgHandler.QueueMessage("Found controller count change in database. Stopping all inputs and reinitializing virtual controllers.");

            //First, stop all inputs
            Task t = InputHndlr.StopAndHaltAllInputs();
            t.Wait();

            //Re-initialize the new number of virtual controllers
            VControllerContainer.VControllerMngr.Dispose();
            VControllerContainer.VControllerMngr.Initialize();
            int acquiredCount = VControllerContainer.VControllerMngr.InitControllers(newJoystickCount);

            //Resume inputs
            InputHndlr.ResumeRunningInputs();

            MsgHandler.QueueMessage($"Set up and acquired {acquiredCount} controllers!");
        }   
    }
}
