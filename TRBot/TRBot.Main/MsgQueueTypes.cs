﻿namespace TRBot.Main
{
    /// <summary>
    /// The types of message queues.
    /// </summary>
    public enum MsgQueueTypes
    {
        Message = 0,
        Command = 1
    }
}
