﻿using System;
using System.Runtime.InteropServices;
using static TRBot.Utilities.TRBotOSPlatform;

namespace TRBot.Main
{
    internal class Program
    {
        [DllImport("libc")]
        private static extern uint geteuid();

        static void Main(string[] args)
        {
            //Set current directory to where the program was run from
            string assemblyLoc = typeof(Program).Assembly.Location;
            Environment.CurrentDirectory = System.IO.Path.GetDirectoryName(assemblyLoc);

            Console.WriteLine($"Set current working directory to: {Environment.CurrentDirectory}");
            
            //Check if running as root and print a warning
            if (CurrentOS == OS.GNULinux)
            {
                uint euid = geteuid();
                
                //Root
                if (euid == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("WARNING!! WARNING!! WARNING!!!");
                    Console.WriteLine();
                    Console.WriteLine("YOU ARE RUNNING TRBOT AS ROOT, WHICH HAS THE POTENTIAL TO CAUSE HARM TO THE FUNCTIONING OF YOUR SYSTEM! UNLESS YOU KNOW THE RISKS, PLEASE RUN TRBOT AS A NORMAL USER.");
                    Console.WriteLine();
                    Console.WriteLine("WARNING!! WARNING!! WARNING!!!");
                    Console.WriteLine();

                    //Wait a bit so they can read the message
                    System.Threading.Thread.Sleep(4000);
                }
            }

            using (BotProgram botProgram = new BotProgram())
            {
                botProgram.Initialize();

                if (botProgram.Initialized == true)
                {
                    botProgram.Run();
                }
                else
                {
                    Console.WriteLine("Bot failed to initialize. Press any key to continue...");
                    Console.ReadKey();
                }
            }
        }
    }
}
