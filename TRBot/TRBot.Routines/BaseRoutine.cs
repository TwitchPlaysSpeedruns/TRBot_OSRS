﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using TRBot.Data;
using TRBot.Logging;
using TRBot.Utilities;
using TRBot.WebSocket;
using TRBot.Events;
using TRBot.VirtualControllers;
using TRBot.Misc;

namespace TRBot.Routines
{
    /// <summary>
    /// The base class for bot routines.
    /// </summary>
    public abstract class BaseRoutine
    {
        public bool Enabled = true;
        public string ClassName = string.Empty;
        public bool ResetOnReload = false;
        public string ValueStr = string.Empty;

        protected IRoutineHandler RoutineHandler = null;
        protected ITRBotLogger Logger = null;
        protected IDatabaseManager<BotDBContext> DatabaseMngr = null;
        protected IDataReloader DataReloader = null;
        protected IBotMessageHandler MessageHandler = null;
        protected IVirtualControllerContainer VControllerContainer = null;
        protected IEventDispatcher EvtDispatcher = null;
        protected IWebSocketManager WebSocketMngr = null;
        protected IInputHandler InputHndlr = null;

        public BaseRoutine()
        {

        }

        /// <summary>
        /// Sets data required for many routines to function.
        /// </summary>
        public void SetRequiredData(IRoutineHandler routineHandler, ITRBotLogger logger,
            IDatabaseManager<BotDBContext> databaseMngr, IBotMessageHandler messageHandler,
            IDataReloader dataReloader, IVirtualControllerContainer vControllerContainer,
            IEventDispatcher evtDispatcher, IWebSocketManager webSocketMngr, IInputHandler inputHandler)
        {
            RoutineHandler = routineHandler;
            Logger = logger;
            DatabaseMngr = databaseMngr;
            MessageHandler = messageHandler;
            DataReloader = dataReloader;
            VControllerContainer = vControllerContainer;
            EvtDispatcher = evtDispatcher;
            WebSocketMngr = webSocketMngr;
            InputHndlr = inputHandler;
        }

        public virtual void Initialize()
        {

        }

        public virtual void CleanUp()
        {
            RoutineHandler = null;
        }

        public abstract void UpdateRoutine(in DateTime currentTimeUTC);
    }
}
