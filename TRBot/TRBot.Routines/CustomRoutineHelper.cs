﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reflection;
using TRBot.Connection;
using TRBot.Consoles;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.CodeAnalysis.CSharp.Scripting;

namespace TRBot.Routines
{
    /// <summary>
    /// Helper for custom bot routines.
    /// </summary>
    public static class CustomRoutineHelper
    {
        //Add references and assemblies
        private static readonly Assembly[] CustomRoutineReferences = new Assembly[]
        {
            typeof(Console).Assembly,
            typeof(List<int>).Assembly,
            typeof(IClientService).Assembly,
            typeof(GameConsole).Assembly,
            typeof(TRBot.Data.CommandData).Assembly,
            typeof(TRBot.Logging.TRBotLogger).Assembly,
            typeof(TRBot.Misc.IBotMessageHandler).Assembly,
            typeof(Parsing.IParser).Assembly,
            typeof(TRBot.Permissions.PermissionAbility).Assembly,
            typeof(TRBot.Routines.BaseRoutine).Assembly,
            typeof(TRBot.Utilities.EnumUtility).Assembly,
            typeof(VirtualControllers.IVirtualController).Assembly,
        };

        private static readonly string[] CustomRoutineImports = new string[]
        {
            nameof(System),
            $"{nameof(System)}.{nameof(System.Collections)}.{nameof(System.Collections.Generic)}",
            Assembly.GetExecutingAssembly().GetName().Name,
            $"{nameof(TRBot)}.{nameof(TRBot.Parsing)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Consoles)}",
            $"{nameof(TRBot)}.{nameof(TRBot.VirtualControllers)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Connection)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Misc)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Data)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Utilities)}"
        };

        private static ScriptOptions ScriptCompileOptions = ScriptOptions.Default.WithReferences(CustomRoutineReferences).WithImports(CustomRoutineImports);

        /// <summary>
        /// Creates a custom routine given code to run.
        /// The given code must define a new routine type and return an instance of it.
        /// </summary>
        /// <param name="code">The code to run for the script.
        /// The code must define a new routine type deriving from <see cref="BaseRoutine"/> and return an instance of it.</param>
        /// <returns>A Task of CustomRoutineCreationData, containing the new routine and an error message.</returns>
        public static async Task<CustomRoutineCreationData> CreateCustomRoutine(string code)
        {
            try
            {
                //Compile the code
                Script<BaseRoutine> routine = CSharpScript.Create<BaseRoutine>(code, ScriptCompileOptions, null, null);
                routine.Compile();

                //Run it and return a new routine
                ScriptState<BaseRoutine> newRoutine = await routine.RunAsync();

                //No return value
                if (newRoutine.ReturnValue == null)
                {
                    return new CustomRoutineCreationData(null, "Custom code does not return a valid routine.");
                }

                return new CustomRoutineCreationData(newRoutine.ReturnValue, string.Empty);
            }
            catch (CompilationErrorException exception)
            {
                return new CustomRoutineCreationData(null, $"Compiler error on custom routine. {exception.Message}");
            }
            catch (Exception otherExc)
            {
                return new CustomRoutineCreationData(null, $"Exec runtime error. {otherExc.Message}");
            }
        }
    }
}
