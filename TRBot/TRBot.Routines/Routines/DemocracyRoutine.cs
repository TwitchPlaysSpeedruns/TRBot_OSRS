﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using TRBot.Parsing;
using TRBot.Consoles;
using TRBot.Misc;
using TRBot.Events;
using TRBot.Data;

namespace TRBot.Routines
{
    /// <summary>
    /// A routine handling inputs for democracy input modes.
    /// </summary>
    public class DemocracyRoutine : BaseRoutine
    {
        /// <summary>
        /// The duration of the voting period.
        /// </summary>
        public long VotingDuration { get; private set; } = 0L;

        private DateTime NextInputTime = default;
        
        /// <summary>
        /// Inputs sent by each user. These must be valid inputs run through post-process methods.
        /// The key is the input list and the value is the count.
        /// </summary>
        private ConcurrentDictionary<List<List<ParsedInput>>, long> AllInputs = new ConcurrentDictionary<List<List<ParsedInput>>, long>(Environment.ProcessorCount, 64, new InputListComparer());

        /// <summary>
        /// The cached democracy resolution mode when voting started.
        /// This value will be used for the entire vote regardless if the setting was modified afterwards.
        /// </summary>
        private DemocracyResolutionModes StartResMode = DemocracyResolutionModes.ExactInput;

        public DemocracyRoutine(in long votingDuration)
        {
            SetVoteDuration(votingDuration);
        }

        public void SetVoteDuration(in long votingDuration)
        {
            VotingDuration = votingDuration;
        }

        public override void Initialize()
        {
            base.Initialize();

            DateTime nowUTC = DateTime.UtcNow;
            NextInputTime = nowUTC;

            InputHndlr.InputsHaltedEvent -= OnInputsHalted;
            InputHndlr.InputsHaltedEvent += OnInputsHalted;

            DataReloader.SoftDataReloadedEvent -= OnDataReload;
            DataReloader.SoftDataReloadedEvent += OnDataReload;

            DataReloader.HardDataReloadedEvent -= OnDataReload;
            DataReloader.HardDataReloadedEvent += OnDataReload;
        }

        public override void CleanUp()
        {
            InputHndlr.InputsHaltedEvent -= OnInputsHalted;
            DataReloader.SoftDataReloadedEvent -= OnDataReload;
            DataReloader.HardDataReloadedEvent -= OnDataReload;

            base.CleanUp();
        }

        private void OnInputsHalted()
        {
            //Clear all inputs when halted
            //Inputs are often halted when switching consoles, virtual controller count, and virtual controller type
            AllInputs.Clear();
        }

        private void OnDataReload()
        {
            //Update voting time to the new value
            VotingDuration = DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.DEMOCRACY_VOTE_TIME, 10000L);
        }

        /// <summary>
        /// Adds an input sequence to be considered for voting.
        /// </summary>
        public void AddInputSequence(string userName, string originalInputMsg, List<List<ParsedInput>> inputList)
        {
            if (inputList == null || inputList.Count == 0
                || inputList[0] == null || inputList[0].Count == 0)
            {
                Logger.Error($"Invalid input list added by {userName}!");
                return;
            }

            //Get the resolution mode
            if (AllInputs.Count == 0)
            {
                DemocracyResolutionModes resolutionMode = (DemocracyResolutionModes)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.DEMOCRACY_RESOLUTION_MODE, 0L);

                if (resolutionMode < 0 || resolutionMode > DemocracyResolutionModes.ExactInput)
                {
                    resolutionMode = DemocracyResolutionModes.ExactSequence;
                    MessageHandler.QueueMessage($"Democracy resolution mode is invalid! Setting resolution mode to {DemocracyResolutionModes.ExactSequence}");

                    //Change the mode in the database
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        Settings demResSetting = DataHelper.GetSettingNoOpen(SettingsConstants.DEMOCRACY_RESOLUTION_MODE, context);
                        if (demResSetting == null)
                        {
                            demResSetting = new Settings(SettingsConstants.DEMOCRACY_RESOLUTION_MODE, string.Empty, 0L);
                        }

                        demResSetting.ValueInt = (long)DemocracyResolutionModes.ExactSequence;

                        context.SaveChanges();
                    }
                }

                StartResMode = resolutionMode;
            }

            //Modify the passed in input sequence based on the democracy resolution type
            //Resolving now makes it possible to simply choose the input with the most votes when the time comes
            List<List<ParsedInput>> resolvedInpList = inputList;
            string inputMsg = originalInputMsg;

            if (StartResMode != DemocracyResolutionModes.ExactSequence)
            {
                //The input should have been validated beforehand, so the console must be valid
                GameConsole console = DataHelper.GetCurrentConsole(DatabaseMngr);
                int defaultPort = 0;
                int defaultDur = (int)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.DEFAULT_INPUT_DURATION, 200L);

                User user = DataHelper.GetUser(DatabaseMngr, userName);
                if (user != null)
                {
                    defaultPort = (int)user.ControllerPort;
                }

                if (StartResMode == DemocracyResolutionModes.ExactInput)
                {
                    resolvedInpList = new List<List<ParsedInput>>() { new List<ParsedInput>() { inputList[0][0] } };
                }
                else if (StartResMode == DemocracyResolutionModes.SameName)
                {
                    ParsedInput input = ParsedInput.Default(defaultDur);
                    input.Name = inputList[0][0].Name;

                    resolvedInpList = new List<List<ParsedInput>>() { new List<ParsedInput>() { input } };
                }

                inputMsg = ReverseParser.ReverseParse(new ParsedInputSequence()
                        { 
                            Inputs = resolvedInpList, ParsedInputResult = ParsedInputResults.Valid
                        },
                        console,
                        new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowNonDefaultPorts,
                            defaultPort, ReverseParser.ShowDurationTypes.ShowNonDefaultDurations, defaultDur)
                        );
            }

            if (AllInputs.TryGetValue(resolvedInpList, out long count) == false)
            {
                count = 1;
                AllInputs[resolvedInpList] = count;
            }
            else
            {
                count += 1;
                AllInputs[resolvedInpList] = count;
            }

            EvtDispatcher.DispatchEvent(EventTypes.INPUT_VOTE,
                new InputVoteEventData(userName, inputMsg, count));

            //Logger.Information($"Added input sequence for user {userName}. String: \"{inputMsg}\" | Count: {AllInputs[resolvedInpList]} | Total: {AllInputs.Count} | Mode: {StartResMode}");
        }

        public override void UpdateRoutine(in DateTime currentTimeUTC)
        {
            //Update the time if we have no inputs
            //This refreshes the duration so inputs consistently go through
            if (AllInputs.Count == 0)
            {
                NextInputTime = currentTimeUTC;
                return;
            }

            //Get time difference and time remaining
            TimeSpan diff = currentTimeUTC - NextInputTime;

            //Not enough time passed - return
            if (diff.TotalMilliseconds < VotingDuration)
            {
                return;
            }

            //Update next input time
            NextInputTime = currentTimeUTC;

            //Logger.Information($"Passed voting duration of {VotingDuration}");

            List<List<ParsedInput>> executedInputList = ResolveSequence();

            //Clear all inputs in our main list now that they're in the dictionary by count
            AllInputs.Clear();

            //Something happened while resolving inputs, so return
            //We should have printed a detailed message beforehand
            if (executedInputList == null)
            {
                return;
            }

            //Get the current console
            int lastConsoleID = (int)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.LAST_CONSOLE, 1L);
            GameConsole usedConsole = DataHelper.GetCurrentConsole(DatabaseMngr);

            if (usedConsole == null)
            {
                MessageHandler.QueueMessage("The current console is somehow null! Cannot execute input.");
                return;
            }

            //Get the final input string
            int defaultDur = (int)DataHelper.GetSettingInt(DatabaseMngr, SettingsConstants.DEFAULT_INPUT_DURATION, 200L);
            string finalInputStr = ReverseParser.ReverseParse(new ParsedInputSequence()
                        { 
                            Inputs = executedInputList, ParsedInputResult = ParsedInputResults.Valid
                        },
                        usedConsole,
                        new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowAllPorts,
                            0, ReverseParser.ShowDurationTypes.ShowNonDefaultDurations, defaultDur)
                        );

            EvtDispatcher.DispatchEvent(EventTypes.INPUT_VOTE_END, new InputVoteEndEventData(finalInputStr));

            //Make sure inputs aren't stopped
            if (InputHndlr.InputsHalted == true)
            {
                //We can't process inputs because they're currently stopped
                MessageHandler.QueueMessage("New inputs cannot be processed until all other inputs have stopped.");
                return;
            }

            ///At this point, the input is safe to perform
            ///If we switched controller counts, <see cref="OnInputsHalted" /> would clear the input list
            ///If it's changed in the database, it won't apply until a data reload
            ///The reload will again trigger <see cref="OnInputsHalted" />
            ///Don't bother checking if a user is no longer able to perform inputs entered in the sequence
            ///It's fair game since they were able to enter it then

            /*************************
            * Execute the input now! *
            *************************/

            InputHndlr.CarryOutInput(executedInputList, usedConsole, VControllerContainer.VControllerMngr);
        }

        private List<List<ParsedInput>> ResolveSequence()
        {
            //Choose the input sequence with the most votes

            List<List<ParsedInput>> chosenList = null;
            long maxCount = -1;

            foreach (KeyValuePair<List<List<ParsedInput>>, long> kvPair in AllInputs)
            {
                if (kvPair.Value > maxCount)
                {
                    chosenList = kvPair.Key;
                    maxCount = kvPair.Value;
                }
            }

            return chosenList;
        }

        /// <summary>
        /// An <see cref="IEqualityComparer{T}" /> for an input sequence's input list. This compares the length and contents of the lists.
        /// </summary>
        private class InputListComparer : IEqualityComparer<List<List<ParsedInput>>>
        {
            public bool Equals(List<List<ParsedInput>> inputList1, List<List<ParsedInput>> inputList2)
            {
                //Both null - equal
                if (inputList1 == null && inputList2 == null)
                {
                    return true;
                }

                //One is null and the other isn't - not equal
                if ((inputList1 == null && inputList2 != null) 
                    || (inputList1 != null && inputList2 == null))
                {
                    return false;
                }

                //Check for count
                if (inputList1.Count != inputList2.Count)
                {
                    return false;
                }

                for (int i = 0; i < inputList1.Count; i++)
                {
                    List<ParsedInput> inpList1 = inputList1[i];
                    List<ParsedInput> inpList2 = inputList2[i];

                    //Check for count
                    if (inpList1.Count != inpList2.Count)
                    {
                        return false;
                    }

                    //Compare each individual parsed input in each list
                    for (int j = 0; j < inpList1.Count; j++)
                    {
                        ParsedInput inp1 = inpList1[j];
                        ParsedInput inp2 = inpList2[j];

                        if (inp1 != inp2)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }

            public int GetHashCode(List<List<ParsedInput>> inputList)
            {
                unchecked
                {
                    //Get the combined hash of each input
                    int hash = 0;
                    for (int i = 0; i < inputList.Count; i++)
                    {
                        List<ParsedInput> inpList = inputList[i];
                        for (int j = 0; j < inpList.Count; j++)
                        {
                            hash += inpList[j].GetHashCode();
                        }
                    }

                    return hash;
                }
            }
        }
    }
}
