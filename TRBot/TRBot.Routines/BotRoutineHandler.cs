﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using TRBot.Data;
using TRBot.Logging;
using TRBot.WebSocket;
using TRBot.Events;
using TRBot.Misc;
using TRBot.VirtualControllers;
using TRBot.Utilities;

namespace TRBot.Routines
{
    /// <summary>
    /// Handles bot routines.
    /// </summary>
    public class RoutineHandler : IRoutineHandler
    {
        private readonly ConcurrentDictionary<string, BaseRoutine> BotRoutines = new ConcurrentDictionary<string, BaseRoutine>(Environment.ProcessorCount * 2, 8);

        private ITRBotLogger Logger = null;
        private IDatabaseManager<BotDBContext> DatabaseMngr = null;
        private IDataReloader DataReloader = null;
        private IBotMessageHandler MessageHandler = null;
        private IVirtualControllerContainer VControllerContainer = null;
        private IEventDispatcher EvtDispatcher = null;
        private IWebSocketManager WebSocketMngr = null;
        private IInputHandler InputHndlr = null;

        /// <summary>
        /// Additional assemblies to look in when adding routines.
        /// This is useful if the routine's Type is outside this assembly.
        /// </summary>
        private Assembly[] AdditionalAssemblies = Array.Empty<Assembly>();

        public RoutineHandler(IDatabaseManager<BotDBContext> databaseMngr, ITRBotLogger logger,
            IDataReloader dataReloader, IBotMessageHandler msgHandler,
            IVirtualControllerContainer vControllerContainer, IEventDispatcher evtDispatcher,
            IWebSocketManager webSocketMngr, IInputHandler inputHandler,
            Assembly[] additionalAssemblies)
        {
            DatabaseMngr = databaseMngr;
            Logger = logger;
            DataReloader = dataReloader;
            MessageHandler = msgHandler;
            VControllerContainer = vControllerContainer;
            EvtDispatcher = evtDispatcher;
            WebSocketMngr = webSocketMngr;
            InputHndlr = inputHandler;
            AdditionalAssemblies = additionalAssemblies;
        }

        public void Initialize()
        {
            DataReloader.SoftDataReloadedEvent -= OnDataReloadedSoft;
            DataReloader.SoftDataReloadedEvent += OnDataReloadedSoft;

            DataReloader.HardDataReloadedEvent -= OnDataReloadedHard;
            DataReloader.HardDataReloadedEvent += OnDataReloadedHard;

            PopulateRoutinesFromDB();
        }

        public void CleanUp()
        {
            DataReloader.SoftDataReloadedEvent -= OnDataReloadedSoft;
            DataReloader.HardDataReloadedEvent -= OnDataReloadedHard;

            CleanUpRoutines();
        }

        private void CleanUpRoutines()
        {
            foreach (KeyValuePair<string, BaseRoutine> routine in BotRoutines)
            {
                routine.Value.CleanUp();
            }
        }

        private void PopulateRoutinesFromDB()
        {
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                foreach (RoutineData routineData in context.Routines)
                {
                    AddRoutine(routineData.Name, routineData.ClassName, routineData.ValueStr, routineData.Enabled > 0, routineData.ResetOnReload > 0);
                }
            }
        }

        private void OnDataReloadedSoft()
        {
            UpdateRoutinesFromDB();
        }

        private void OnDataReloadedHard()
        {
            //Clean up routines only if they hard reload
            KeyValuePair<string, BaseRoutine>[] allRoutines = BotRoutines.ToArray();
            
            for (int i = 0; i < allRoutines.Length; i++)
            {
                KeyValuePair<string, BaseRoutine> keyValue = allRoutines[i];
                BaseRoutine baseRoutine = keyValue.Value;

                if (baseRoutine == null || baseRoutine.ResetOnReload == false)
                {
                    continue;
                }

                RemoveRoutine(keyValue.Key);
            }

            UpdateRoutinesFromDB();
        }

        private void UpdateRoutinesFromDB()
        {
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                List<string> encounteredRoutines = new List<string>(context.Routines.Count());

                foreach (RoutineData routineData in context.Routines)
                {
                    string routineName = routineData.Name;
                    if (BotRoutines.TryGetValue(routineName, out BaseRoutine baseRoutine) == true)
                    {
                        //Remove this routine if the type name is different so we can reconstruct it
                        if (baseRoutine.ClassName != routineData.ClassName)
                        {
                            RemoveRoutine(routineName);

                            baseRoutine = null;
                        }
                    }

                    //Add this routine if it doesn't exist and should
                    if (baseRoutine == null)
                    {
                        //Add this routine
                        AddRoutine(routineName, routineData.ClassName, routineData.ValueStr,
                            routineData.Enabled > 0, routineData.ResetOnReload > 0 );
                    }
                    else
                    {
                        baseRoutine.Enabled = routineData.Enabled > 0;
                        baseRoutine.ClassName = routineData.ClassName;
                        baseRoutine.ResetOnReload = routineData.ResetOnReload > 0;
                        baseRoutine.ValueStr = routineData.ValueStr;
                    }

                    encounteredRoutines.Add(routineName);
                }

                //Remove routines that are no longer in the database
                foreach (string routine in BotRoutines.Keys)
                {
                    if (encounteredRoutines.Contains(routine) == false)
                    {
                        RemoveRoutine(routine);
                    }
                }
            }
        }

        public void Update(in DateTime nowUTC)
        {
            //Update routines
            foreach (BaseRoutine routine in BotRoutines.Values)
            {
                if (routine == null || routine.Enabled == false)
                {
                    continue;
                }

                routine.UpdateRoutine(nowUTC);
            }
        }

        public bool AddRoutine(string routineName, string routineTypeName, string valueStr,
            in bool routineEnabled, in bool resetOnReload)
        {
            //Custom routine - load it
            if (string.IsNullOrEmpty(routineTypeName) == true)
            {
                BaseRoutine customRoutine = LoadCustomRoutine(valueStr);
                if (customRoutine == null)
                {
                    MessageHandler.QueueMessage($"Unable to add routine \"{routineName}\": Custom code failed.");
                    return false;
                }

                customRoutine.Enabled = routineEnabled;
                customRoutine.ClassName = routineTypeName;
                customRoutine.ResetOnReload = resetOnReload;
                customRoutine.ValueStr = valueStr;

                return AddRoutine(routineName, customRoutine);
            }

            Type routineType = Type.GetType(routineTypeName, false, true);
            if (routineType == null && AdditionalAssemblies?.Length > 0)
            {
                //Look for the type in our other assemblies
                for (int i = 0; i < AdditionalAssemblies.Length; i++)
                {
                    Assembly asm = AdditionalAssemblies[i];

                    routineType = asm.GetType(routineTypeName, false, true);
                    
                    if (routineType != null)
                    {
                        Logger.Debug($"Found \"{routineTypeName}\" in assembly \"{asm.GetName()}\"!");
                        break;
                    }
                }                
            }

            if (routineType == null)
            {
                MessageHandler.QueueMessage($"Cannot find routine type \"{routineTypeName}\" for routine \"{routineName}\" in all provided assemblies.");
                return false;
            }

            BaseRoutine routine = null;

            //Try to create an instance
            try
            {
                routine = (BaseRoutine)Activator.CreateInstance(routineType, Array.Empty<object>());
                routine.Enabled = routineEnabled;
                routine.ClassName = routineTypeName;
                routine.ResetOnReload = resetOnReload;
                routine.ValueStr = valueStr;
            }
            catch (Exception e)
            {
                MessageHandler.QueueMessage($"Unable to add routine \"{routineName}\": \"{e.Message}\"");
            }

            return AddRoutine(routineName, routine);
        }

        public bool AddRoutine(string routineName, BaseRoutine routine)
        {
            if (routine == null)
            {
                Logger.Warning("Cannot add null routine.");
                return false;
            }

            //Clean up the existing routine before overwriting it with the new value
            if (BotRoutines.TryGetValue(routineName, out BaseRoutine existingRoutine) == true)
            {
                existingRoutine.CleanUp();
            }

            BotRoutines[routineName] = routine;
            routine.SetRequiredData(this, Logger, DatabaseMngr, MessageHandler, DataReloader,
                VControllerContainer, EvtDispatcher, WebSocketMngr, InputHndlr);
            routine.Initialize();

            return true;
        }

        public bool RemoveRoutine(string routineName)
        {
            bool removed = BotRoutines.Remove(routineName, out BaseRoutine routine);

            if (removed == true)
            {
                routine?.CleanUp();
            }

            return removed;
        }

        public bool RemoveRoutine(BaseRoutine routine)
        {
            string routineName = string.Empty;
            bool found = false;

            //Look for the reference
            foreach (KeyValuePair<string, BaseRoutine> kvPair in BotRoutines)
            {
                if (kvPair.Value == routine)
                {
                    routineName = kvPair.Key;
                    found = true;
                    break;
                }
            }

            //The routine was found, so remove it
            if (found == true)
            {
                return RemoveRoutine(routineName);
            }

            return false;
        }

        public BaseRoutine FindRoutine(string name)
        {
            BotRoutines.TryGetValue(name, out BaseRoutine routine);
            
            return routine;
        }

        public T FindRoutine<T>() where T : BaseRoutine
        {
            foreach (BaseRoutine routine in BotRoutines.Values)
            {
                if (routine is T typeRoutine)
                {
                    return typeRoutine;
                }
            }

            return null;
        }

        private BaseRoutine LoadCustomRoutine(string filePath)
        {
            //First try to read the file as an absolute path
            string codeText = FileHelpers.ReadFromTextFile(filePath);

            //If that wasn't found, try a relative path
            if (string.IsNullOrEmpty(codeText) == true)
            {
                codeText = FileHelpers.ReadFromTextFile(DataConstants.DataFolderPath, filePath);
            }

            if (string.IsNullOrEmpty(codeText) == true)
            {
                MessageHandler.QueueMessage("Invalid source file for custom routine. Double check its location.", Serilog.Events.LogEventLevel.Warning);
                return null;
            }

            Logger.Information($"Compiling custom routine from code file \"{filePath}\"...");

            //Find a way to do this async down the road so it doesn't halt the thread
            Task<CustomRoutineCreationData> cstmRoutCTask = CustomRoutineHelper.CreateCustomRoutine(codeText);
            cstmRoutCTask.Wait();

            CustomRoutineCreationData cstmRoutCData = cstmRoutCTask.Result;

            //Print error message
            if (string.IsNullOrEmpty(cstmRoutCData.ErrorMessage) == false)
            {
                MessageHandler.QueueMessage(cstmRoutCData.ErrorMessage, Serilog.Events.LogEventLevel.Warning);
                return null;
            }

            //Return the routine
            return cstmRoutCData.NewRoutine;
        }
    }
}
