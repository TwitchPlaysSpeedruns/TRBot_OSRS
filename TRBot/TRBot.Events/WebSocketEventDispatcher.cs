﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using TRBot.Logging;
using Newtonsoft.Json;
using TRBot.WebSocket;

namespace TRBot.Events
{
    /// <summary>
    /// Dispatches events through a WebSocket.
    /// </summary>
    public class WebSocketEventDispatcher : IEventDispatcher
    {
        public bool Enabled { get; private set; } = true;

        public string SocketPath { get; private set; } = "/evt";
        private IWebSocketManager WebSocketMngr = null;
        private ITRBotLogger Logger = null;

        public WebSocketEventDispatcher(string socketPath, IWebSocketManager webSocketMngr, ITRBotLogger logger)
        {
            WebSocketMngr = webSocketMngr;

            SocketPath = socketPath;

            Logger = logger;

            WebSocketMngr.AddServerService<EventDispatcherSocketService>(SocketPath);
        }

        public void Dispose()
        {
            WebSocketMngr.RemoveServerService(SocketPath);
        }

        public void EnableDispatcher()
        {
            if (Enabled == true)
            {
                Logger.Information($"Cannot enable {nameof(WebSocketEventDispatcher)} since it's already enabled.");
                return;
            }

            Enabled = WebSocketMngr.AddServerService<EventDispatcherSocketService>(SocketPath);
        }

        public void DisableDispatcher()
        {
            if (Enabled == false)
            {
                Logger.Information($"Cannot disable {nameof(WebSocketEventDispatcher)} since it's already disabled.");
                return;
            }

            Enabled = !WebSocketMngr.RemoveServerService(SocketPath);
        }

        public void DispatchEvent(string eventType, BaseEventData eventData)
        {
            if (Enabled == false || WebSocketMngr.IsServerListening == false)
            {
                return;
            }

            try
            {
                EventResponse evtResponse = new EventResponse(eventType, eventData);

                string json = JsonConvert.SerializeObject(evtResponse, Formatting.None);

                //Send the data over the websocket
                WebSocketMngr.ServerBroadcastAsync(SocketPath, json, null, null);
            }
            catch (Exception e)
            {
                Logger.Warning($"Issue broadcasting WebSocket message for {nameof(WebSocketEventDispatcher)}. {e.Message}");
            }
        }
    }
}
