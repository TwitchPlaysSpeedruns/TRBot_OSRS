﻿/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Events
{
    /// <summary>
    /// Event data.
    /// </summary>
    public class BaseEventData : EventArgs
    {
        public BaseEventData()
        {

        }
    }

    public class MessageEventData : BaseEventData
    {
        public string Username = string.Empty;
        public long UserLevel = 0L;
        public string Message = string.Empty;

        public MessageEventData()
        {

        }

        public MessageEventData(string username, in long userLevel, string message)
        {
            Username = username;
            UserLevel = userLevel;
            Message = message;
        }
    }

    public class InputEventData : BaseEventData
    {
        public string Username = string.Empty;
        public long UserLevel = 0L;
        public string InputStr = string.Empty;

        public InputEventData()
        {

        }

        public InputEventData(string username, in long userLevel, string inputStr)
        {
            Username = username;
            UserLevel = userLevel;
            InputStr = inputStr;
        }
    }

    public class CommandEventData : BaseEventData
    {
        public string Username = string.Empty;
        public string Command = string.Empty;
        public string CommandArgs = string.Empty;

        public CommandEventData()
        {

        }

        public CommandEventData(string username, string command, string commandArgs)
        {
            Username = username;
            Command = command;
            CommandArgs = commandArgs;
        }
    }

    public class InputVoteEventData : BaseEventData
    {
        public string Username = string.Empty;
        public string InputStr = string.Empty;
        public long CurVotes = 0;

        public InputVoteEventData()
        {

        }

        public InputVoteEventData(string username, string inputStr, in long curVotes)
        {
            Username = username;
            InputStr = inputStr;
            CurVotes = curVotes;
        }
    }

    public class InputVoteEndEventData : BaseEventData
    {
        public string ChosenInputStr = string.Empty;

        public InputVoteEndEventData(string chosenInputStr)
        {
            ChosenInputStr = chosenInputStr;
        }
    }

    public class InputModeVoteEventData : BaseEventData
    {
        public string Username = string.Empty;
        public string InputMode = string.Empty;
        public long CurVotes = 0;

        public InputModeVoteEventData(string username, string inputMode, in long curVotes)
        {
            Username = username;
            InputMode = inputMode;
            CurVotes = curVotes;
        }
    }

    public class InputModeVoteEndEventData : BaseEventData
    {
        public string WinningInputMode = string.Empty;
        public long WinningVotes = 0;

        public InputModeVoteEndEventData(string winningInputMode, in long winningVotes)
        {
            WinningInputMode = winningInputMode;
            WinningVotes = winningVotes;
        }
    }

    public class EventResponse
    {
        public string EventType = string.Empty;
        public BaseEventData EventData = null;

        public EventResponse()
        {

        }

        public EventResponse(string eventType, BaseEventData eventData)
        {
            EventType = eventType;
            EventData = eventData;
        }
    }
}
