/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using TRBot.Parsing;
using TRBot.Consoles;
using TRBot.Misc;
using TRBot.VirtualControllers;
using TRBot.Logging;

namespace TRBot.Tests.Benchmarks
{
    [MemoryDiagnoser]
    public class InputValidationBenchmarks
    {
        private ITRBotLogger DummyLogger = new DummyLogger();

        public InputValidationBenchmarks()
        {
            
        }

        [Benchmark]
        public void RunNewValidationFailControllerPort()
        {
            RunNewValidation(GetFailControllerPortMessage(), GetSuccessConsole(), 0, 0, GetSuccessRestrictedInputs());
        }

        [Benchmark]
        public void RunNewValidationFailRestrictedInputs()
        {
            RunNewValidation(GetFailBtnComboMessage(), GetSuccessConsole(), 0, 1, GetFailRestrictedInputs());
        }

        [Benchmark]
        public void RunNewValidationFailBtnCombo()
        {
            RunNewValidation(GetFailBtnComboMessage(), GetFailBtnComboConsole(), 0, 1, GetSuccessRestrictedInputs());
        }

        [Benchmark]
        public void RunNewValidationFailInputLvl()
        {
            RunNewValidation(GetSuccessMessage(), GetFailInputLvlConsole(), 0, 1, GetSuccessRestrictedInputs());
        }

        [Benchmark]
        public void RunNewValidationSuccess()
        {
            RunNewValidation(GetSuccessMessage(), GetSuccessConsole(), 0, 1, GetSuccessRestrictedInputs());
        }

        private void RunNewValidation(string message, GameConsole usedConsole, in long userLevel,
            in int maxPortNum, Dictionary<string, int> userRestrictedInputs)
        {
            IVirtualControllerManager dummyVConMngr = new DummyControllerManager(DummyLogger);
            dummyVConMngr.Initialize();
            dummyVConMngr.InitControllers(4);

            StandardParserValidator validator = new StandardParserValidator(userRestrictedInputs,
                userLevel, usedConsole, dummyVConMngr, DummyLogger);

            StandardParser standardParser = StandardParser.CreateStandard(null, null,
                usedConsole.GetInputNames(), 0, maxPortNum, 200, 60000, true, validator);

            //Parse inputs to get our parsed input sequence
            ParsedInputSequence inputSequence = standardParser.ParseInputs(message);

            if (inputSequence.ParsedInputResult == ParsedInputResults.Invalid)
            {
                return;
            }

            //Get a blank input
            InputData blankInpData = usedConsole.GetAvailableBlankInput(userLevel, userRestrictedInputs);

            if (blankInpData != null)
            {
                MidInputDelayData midInputDelayData = ParserPostProcess.InsertMidInputDelays(inputSequence,
                    0, 0, usedConsole, blankInpData);
                
                //If it's successful, replace the input list and duration
                if (midInputDelayData.Success == true)
                {
                    int oldDur = inputSequence.TotalDuration;
                    inputSequence.Inputs = midInputDelayData.NewInputs;
                    inputSequence.TotalDuration = midInputDelayData.NewTotalDuration;
                }
            }
        }

        private string GetFailControllerPortMessage()
        {
            return "&3_right500ms [x300ms .]*10 _x [&2up17ms #17ms]*2 _y1s [up34ms#34ms]*4 start+b+left250ms -right [y34ms #34ms]*3";
        }

        private Dictionary<string, int> GetFailRestrictedInputs()
        {
            return new Dictionary<string, int>()
            {
                { "up", 1 },
                { "b", 1 },
            };
        }

        private string GetFailBtnComboMessage()
        {
            //                                                                    Fails here
            //                                                                         v
            return "_right500ms [x300ms .]*10 _x [up17ms #17ms]*2 _y1s [up34ms#34ms]*4 start+b+left250ms -right [y34ms #34ms]*3";
        }

        private GameConsole GetFailBtnComboConsole()
        {
            return new GameConsole("failbtncombo", new List<InputData>()
            {
                InputData.CreateButton("a", 0),
                InputData.CreateButton("b", 1),
                InputData.CreateButton("x", 2),
                InputData.CreateButton("y", 3),
                InputData.CreateButton("start", 4),
                InputData.CreateAxis("left", 0, 0.5d, 0d, 100d, 0.5d),
                InputData.CreateAxis("right", 0, 0.5d, 1d, 100d, 0.5d),
                InputData.CreateAxis("up", 1, 0.5d, 0d, 100d, 0.5d),
                InputData.CreateAxis("down", 1, 0.5d, 1d, 100d, 0.5d),
                InputData.CreateBlank("#"),
                InputData.CreateBlank("."),
            },
            new List<InvalidCombo>()
            {
                new InvalidCombo(InputData.CreateButton("x", 2)),
                new InvalidCombo(InputData.CreateButton("y", 3)),
                new InvalidCombo(InputData.CreateButton("start", 4)),
            });
        }

        private GameConsole GetFailInputLvlConsole()
        {
            GameConsole console = new GameConsole("failinputlvl", new List<InputData>()
            {
                InputData.CreateButton("a", 0),
                InputData.CreateButton("b", 1),
                InputData.CreateButton("x", 2),
                InputData.CreateButton("y", 3),
                InputData.CreateButton("start", 4),
                InputData.CreateAxis("left", 0, 0.5d, 0d, 100d, 0.5d),
                InputData.CreateAxis("right", 0, 0.5d, 1d, 100d, 0.5d),
                InputData.CreateAxis("up", 1, 0.5d, 0d, 100d, 0.5d),
                InputData.CreateAxis("down", 1, 0.5d, 1d, 100d, 0.5d),
                InputData.CreateBlank("#"),
                InputData.CreateBlank("."),
            }, new List<InvalidCombo>());

            console.ConsoleInputs["up"].Level = 10;

            return console;
        }

        private string GetSuccessMessage()
        {
            return "_right500ms [up300ms .]*10 [x34ms#34ms]*4 -right down+b+left250ms";
        }

        private Dictionary<string, int> GetSuccessRestrictedInputs()
        {
            return new Dictionary<string, int>();
        }

        private GameConsole GetSuccessConsole()
        {
            return new GameConsole("success", new List<InputData>()
            {
                InputData.CreateButton("a", 0),
                InputData.CreateButton("b", 1),
                InputData.CreateButton("x", 2),
                InputData.CreateButton("y", 3),
                InputData.CreateButton("start", 4),
                InputData.CreateAxis("left", 0, 0.5d, 0d, 100d, 0.5d),
                InputData.CreateAxis("right", 0, 0.5d, 1d, 100d, 0.5d),
                InputData.CreateAxis("up", 1, 0.5d, 0d, 100d, 0.5d),
                InputData.CreateAxis("down", 1, 0.5d, 1d, 100d, 0.5d),
                InputData.CreateBlank("#"),
                InputData.CreateBlank("."),
            }, new List<InvalidCombo>());
        }
    }
}