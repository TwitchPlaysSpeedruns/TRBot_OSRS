/* Copyright (C) 2019-2021 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Logging;
using Serilog.Events;

namespace TRBot.Tests
{
    /// <summary>
    /// A dummy logger used in tests.
    /// </summary>
    public class DummyLogger : ITRBotLogger
    {
        /// <summary>
        /// The minimum logging level for the logger.
        /// </summary>
        public LogEventLevel MinLoggingLevel { get; set; }

        /// <summary>
        /// Disposes the logger.
        /// </summary>
        public void Dispose()
        {

        }

        /// <summary>
        /// Logs a verbose log.
        /// </summary>
        public void Verbose(string message)
        {

        }

        /// <summary>
        /// Logs a debug log.
        /// </summary>
        public void Debug(string message)
        {

        }

        /// <summary>
        /// Logs an information log.
        /// </summary>
        public void Information(string message)
        {

        }

        /// <summary>
        /// Logs a warning.
        /// </summary>
        public void Warning(string message)
        {

        }

        /// <summary>
        /// Logs an error.
        /// </summary>
        public void Error(string message)
        {

        }

        /// <summary>
        /// Logs a fatal log.
        /// </summary>
        public void Fatal(string message)
        {

        }
        
        /// <summary>
        /// Writes a log with a given log level.
        /// </summary>
        public void Log(LogEventLevel logEventLevel, string message)
        {

        }
    }
}