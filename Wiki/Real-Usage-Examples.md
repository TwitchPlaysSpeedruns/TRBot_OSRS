TRBot has been used by many streams to date, including [Type2Play](https://www.twitch.tv/type2play), [TwitchPlaysSpeedruns](https://www.twitch.tv/twitchplaysspeedruns), [f1ashko](https://www.twitch.tv/f1ashko), and [BraceYourselfGames](https://www.twitch.tv/braceyourselfgames/clip/SmoggyTangibleChoughEleGiggle) (the developers behind Crypt of the NecroDancer!).

Here are some examples of TRBot in action:

* **Completing Puzzle (Maze Burrow)**
  * https://www.twitch.tv/kimimaru4000/clip/SavoryBraveSandwichJebaited
* **Gnorc Cove Big Skip (Spyro the Dragon)**
  * https://www.twitch.tv/twitchplaysspeedruns/clip/FurtiveIcySwallowLitty
* **Prison 2 Speedrun Sub-57 (Katana ZERO)**
  * https://clips.twitch.tv/ExquisiteDeterminedBubbleteaKeyboardCat-X_nurQac_VsYqfjG
* **Boba Skip (Metal Gear Solid)**
  * https://youtube.com/watch?v=rdOSNKHhvBQ
* **New Guard Manipulation Glitch (Metal Gear Solid)**
  * https://twitter.com/MGSrunners/status/1431503754256388098#m

Have an awesome clip or usage for TRBot? Comment [here](https://codeberg.org/kimimaru/TRBot/issues/89) or submit a PR to add it to this list!
