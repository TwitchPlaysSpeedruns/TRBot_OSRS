# Custom Command and Routine changes in TRBot 2.4+

TRBot 2.4 introduces more dependency injection into the codebase to make it more flexible, cleaner, and easier to test. Several previously global objects were made into instances implementing their interfaces, then constructed at the root of the application and injected as dependencies into the objects requiring them.

Here's what you need to know about migrating custom commands and routines from versions prior to 2.4:

- `DataContainer` has been completely removed. The objects it contained are now accessible via fields in commands and routines.
- `DatabaseManager` is now an instance field called `DatabaseMngr`.
- `InputHandler` is now an instance field called `InputHndlr`.
- The logger `TRBotLogger.Logger` is now an instance field called `Logger`. It has many of the same methods as the Serilog logger, but some are no longer available.
- The virtual controller manager and corresponding virtual controller enum value can be accessed via `VControllerContainer.VControllerMngr` and `VControllerContainer.VControllerType`, respectively.

For example, the following code:

```
DataContainer.MessageHandler.QueueMessage("Test");
```

Turns into this:

```
MessageHandler.QueueMessage("Test");
```

## ExecCommand

Code invoked through the `ExecCommand` also has access to all command fields through the `ThisCmd` field.
